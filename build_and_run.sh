#!/bin/bash
set -e

# Run the 'mvn package' command in the root folder of the project.
# Maven then builds every module of the project, by looking at the root POM.
mvn package

# This creates the composed network of docker containers, by reading the 'docker-compose.yml' file.
# Terminate the composed docker containers with command: 'docker-compose down'
docker-compose up -d --build

# Cleaning up unused images
docker image prune -f

# Waiting a few seconds for the docker containers to start up
sleep 2s

# Going into the client(s) to run their tests (Comment this out for IDE testing, however do NOT push this!)
pushd client
mvn test
popd
