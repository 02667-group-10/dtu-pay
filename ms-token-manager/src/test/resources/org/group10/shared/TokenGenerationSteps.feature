Feature: Token generation


  Scenario: Check format of token
    Given A customer with UserId "02"
    When A token is build
    Then Token should have correct format

    Scenario: Generation of tokens
      Given A customer with UserId "03"
      And Customer has 1 tokens in his wallet
      And Customer requests 5 tokens
      When GenerateTokens function is called
      Then 5 tokens should be generated

# TODO programmet kan ikke builde pga. forbindelsen denne test opretter !
#  Scenario: Verify and create requests for tokens
#    Given A customer with UserId "04"
#    And Customer has 1 tokens in his wallet
#    And Customer requests 5 tokens
#    When Token request message is being handled
#    Then Tokens are sent to RESTApi
