package org.group10.shared;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.JSONObject;
import org.junit.Before;

import java.util.ArrayList;

import static org.group10.shared.TokenManagerJSONEncoder.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * @author Justus Gammelgaard
 */

public class TokenGenerationStep {

    TokenManager tokenManager = new TokenManager(true);
    String userID;
    String builtToken;
    String request;
    int userRequestedTokens;
    int userExistingTokens;
    ArrayList<String> tokens;


    public TokenGenerationStep() throws Exception {
    }




    @Then("Then check function should return {int}")
    public void thenCheckFunctionShouldReturn(int amountOfTokens) {
        assertEquals(userExistingTokens, amountOfTokens);
    }

    @Given("A customer with UserId {string}")
    public void aCustomerWithUserId(String id) {
        this.userID = id;

    }

    @And("Customer has {int} tokens in his wallet")
    public void customerHasTokensInHisWallet(int currentAmountOfTokens) {
        this.userExistingTokens = currentAmountOfTokens;
    }

    @When("A token is build")
    public void aTokenIsBuild() {
        builtToken = tokenManager.buildToken(userID);
    }

    @Then("Token should have correct format")
    public void tokenShouldHaveCorrectFormat() {
        String[] tokenizer = builtToken.split(":");
        int lengthOfUserID = tokenizer[0].length();
        int lengthOfUUID = tokenizer[1].length();

        assertEquals(2, lengthOfUserID);
        assertEquals(36, lengthOfUUID);

    }

    @And("Customer requests {int} tokens")
    public void customerRequestsTokens(int amountOfTokensRequested) {
        this.userRequestedTokens = amountOfTokensRequested;
    }

    @When("GenerateTokens function is called")
    public void generateTokensFunctionIsCalled() throws Exception {
        tokenManager.generateTokens(userID, userRequestedTokens);
        this.tokens = tokenManager.getTokens();
    }

    @Then("{int} tokens should be generated")
    public void tokensShouldBeGenerated(int amountOfTokens) {
        System.out.println(tokens.size());
        assertEquals(tokens.size(), amountOfTokens);
    }

    @When("Token request message is being handled")
    public void tokenRequestMessageIsBeingHandled() {
        request = generateTokensRequest(userID, userRequestedTokens, MQNaming.CustomerAPI);
    }

    @Then("Tokens are sent to RESTApi")
    public void tokensAreSentToUserManager() throws Exception {
        tokenManager.startConnections();
        tokenManager.handleMsg(new JSONObject(request));
    }
}
