package org.group10.shared;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class TokenMain implements QuarkusApplication {


    @Override
    public int run(String... args) throws Exception {
        System.out.println("Starting the TokenManager ...");

        TokenManager TM = new TokenManager(false);

            while(true) {
                System.out.println("Waiting for a request ...");
                String msg = TM.recvMsg();
                System.out.println("Request handled: " + msg);


            }

        }

}
