package org.group10.shared;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

public class TokenManager {
    private Boolean isTesting;

    private MQReceiver mqReceiver;
    private MQSender customerSender;
    ArrayList<String> tokens;


    public TokenManager(Boolean testing) throws Exception {
        isTesting = true;
        if (!testing){
            isTesting = false;
            startConnections();
        }

    }

    /**
     * Starts queues
     */
    public void startConnections() throws Exception {
        mqReceiver = new MQReceiver(MQNaming.TokenManager);
        customerSender = new MQSender(MQNaming.CustomerAPI);
        mqReceiver.startChannel();
    }


    /**
     * @param userID the ID of a user
     * @return a custom made token consisting of userID and and a UUID
     * @formatOfToken <UserID>:<RandomUUID>
     */
    public String buildToken(String userID) {
        return userID + ":" + UUID.randomUUID();
    }

    /**
     * Generates requested amount of tokens
     * @param userID the ID of a user
     * @param numRequestedTokens the amount of tokens requested by a customer/user
     * @throws Exception in case of error
     */
    // Generates 1-5 tokens depending on customers token-wallet
    public ArrayList generateTokens(String userID, int numRequestedTokens) throws Exception {
        this.tokens = new ArrayList<>();

        for(int x = 0; x < numRequestedTokens; x++) {
            tokens.add(buildToken(userID));
        }

        return tokens;



    }



    /**
     * @return List of tokens
     */
    //Used for testing purposes
    public ArrayList<String> getTokens() {
        return tokens;
    }


    /**
     * @return message from queue in JSON format (to print)
     * @throws Exception in case of error
     */
    public String recvMsg() throws Exception {
        String msg = mqReceiver.receive();
        handleMsg(new JSONObject(msg));
        return msg;
    }

    /**
     * Handles the ingoing requests and catches errors in token requests from customers
     * @param msgObject a message from a queue in JSON format
     * @return Proper message
     * @throws Exception in case of error
     */
    public void handleMsg(JSONObject msgObject) throws Exception {
        String ID = msgObject.getString("ID");
        String request = msgObject.getString("request");
        String pack;
        switch (request) {
            case "tokenRequest":

                int reqTokens = Integer.parseInt(msgObject.getString("numRequestedTokens"));
                this.tokens = generateTokens(ID, reqTokens);
                pack = GeneralJSONEncoder.TokensRequest(ID, MQNaming.TokenManager, tokens);
                customerSender.send(pack);

                break;

            default:
                pack = GeneralJSONEncoder.error("Bad Request", MQNaming.TokenManager);
                customerSender.send(pack);

                break;
        }

    }




    /**
     * Listens for messages
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        TokenManager TM = new TokenManager(false);

        while(true) {
            TM.recvMsg();


        }

    }

}
