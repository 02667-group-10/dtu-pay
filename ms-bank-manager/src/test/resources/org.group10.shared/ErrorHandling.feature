Feature: Error handling
  Scenario: Bank account does not have enough money
  Given the customer "Simon" "Anderson" with CPR "111111-7125" has a bank account
  And the balance of the costumer account is 1000
  And the customer is registered with DTUPay
  And the merchant "Mathias" "Kuckles" with CPR number "222222-2741" has a bank account
  And the balance of the merchant account is 2000
  And Payment sends a request to Bank with "111111-7125","222222-2741" and "3000"
  And Bank receives the request
  Then the transfer is unsuccessful


  Scenario: Bank account does not exist
    Given the customer "Oline" "Anderson" with CPR "111111-2222" does not have a bank account
    And the customer is registered with DTUPay
    And the merchant "Mathias" "Kuckles" with CPR number "222222-2741" has a bank account
    And Payment sends a request to Bank with "111115-7125","222222-2741" and "3000"
    And Bank receives the request
    Then the transfer is unsuccessful, since bank account does not exist


