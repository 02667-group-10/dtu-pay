Feature: Payment over MQs
  Scenario: Successful payment from PaymentMangement to BankManagement
    Given the customer "Simon" "Anderson" with CPR "111111-7125" has a bank account
    And the balance of the costumer account is 1000
    And the customer is registered with DTUPay
    And the merchant "Mathias" "Kuckles" with CPR number "222222-2741" has a bank account
    And the balance of the merchant account is 2000
    And Payment sends a request to Bank with "111111-7125","222222-2741" and "10"
    And Bank receives the request
    And the transfer is successful
    Then "111111-7125" has "990" left
    Then "222222-2741" has "2010" left

