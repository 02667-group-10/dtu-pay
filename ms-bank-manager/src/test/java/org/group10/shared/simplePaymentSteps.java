package org.group10.shared;
/**
 * @author: Simon W. Ø. Larsen
 */

import io.cucumber.java.en.And;
import junit.runner.Version;

import org.group10.shared.Payment;
import dtu.ws.fastmoney.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.After;
import org.junit.Assert;

import javax.validation.constraints.AssertTrue;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeoutException;

public class simplePaymentSteps {

    Payment payment = new Payment();
    boolean isPaymentSuccessful;
    BankManagement bankManagement = new BankManagement();


    @Given("the customer {string} {string} with CPR {string} has a bank account")
    public void the_customer_with_cpr_has_a_bank_account(String string, String string2, String string3) throws BankServiceException_Exception {
        payment.setUserCostumer(string,string2,string3);
    }


    @Given("the balance of the costumer account is {int}")
    public void the_balance_of_the_costumer_account_is(Integer int1) throws BankServiceException_Exception {
        payment.createBankAccountCostumer(int1);
    }


    @Given("the customer is registered with DTUPay")
    public void the_customer_is_registered_with_dtu_pay() {
    }


    @Given("the merchant {string} {string} with CPR number {string} has a bank account")
    public void the_merchant_with_cpr_number_has_a_bank_account(String string, String string2, String string3) throws BankServiceException_Exception {
        payment.setUserMerchant(string,string2,string3);
    }


    @Given("the balance of the merchant account is {int}")
    public void the_balance_of_the_merchant_account_is(Integer int1) throws BankServiceException_Exception {
        payment.createBankAccountMerchant(int1);
    }


    @Given("the merchant is registered with DTUPay")
    public void the_merchant_is_registered_with_dtu_pay() {
    }


    @When("the merchant initiates a payment for {string} kr by the customer")
    public void the_merchant_initiates_a_payment_for_kr_by_the_customer(String string) throws BankServiceException_Exception {
        isPaymentSuccessful = payment.transfer(string);
    }


    @Then("the payment is successful")
    public void the_payment_is_successful(){
        if (isPaymentSuccessful) {
            System.out.println("Payment successful");
        } else {
            System.out.println("Payment unsuccessful");
        }
    }


    @Then("the balance of the customer at the bank is {int} kr")
    public void the_balance_of_the_customer_at_the_bank_is_kr(Integer int1) throws BankServiceException_Exception {
        BigDecimal amount = payment.getCostumerBalance();
        Assert.assertEquals(new BigDecimal(990), amount);
    }

    @Then("the balance of the merchant at the bank is {int} kr")
    public void the_balance_of_the_merchant_at_the_bank_is_kr(Integer int1) throws BankServiceException_Exception {
        BigDecimal amount = payment.getMerchantBalance();
        Assert.assertEquals(new BigDecimal(2010),amount);
    }


    @Given("Payment sends a request to Bank with {string},{string} and {string}")
    public void payment_send_a_request_to_bank_with_and(String string, String string2, String int1) throws Exception {
        //Pretend to be payment
        MQSender sendTransferQ = new MQSender(MQNaming.BankManager);
        String message = BankManagerJSONEncoder.transferBank("transferBank",string,string2,int1,MQNaming.PaymentResult);
        sendTransferQ.send(message);
    }


    @Given("Bank receives the request")
    public void bank_receives_the_request() throws Exception {
        bankManagement.startConnections();
        MQReceiver QRecv = bankManagement.getQrecv();
        bankManagement.setMessage(QRecv.receive());
        QRecv.stopChannel();
    }


    @Given("the transfer is successful")
    public void the_transfer_is_successful() throws BankServiceException_Exception {
        bankManagement.handleTransferRequest();
        Assert.assertFalse(bankManagement.getError());
    }


    @Then("{string} has {string} left")
    public void has_left(String CPR, String amount) throws BankServiceException_Exception {
        BigDecimal accountStatus = bankManagement.getBalance(CPR);
        Assert.assertEquals(new BigDecimal(amount),accountStatus);
    }


    @Then("the transfer is unsuccessful")
    public void transfer_is_unsuccessful() throws BankServiceException_Exception {
        bankManagement.handleTransferRequest();

        Assert.assertEquals("Transfer unsuccessful", bankManagement.getErrorMessage());
    }


    @Given("the customer {string} {string} with CPR {string} does not have a bank account")
    public void theCustomerWithCPRDoesNotHaveABankAccount(String arg0, String arg1, String arg2) throws BankServiceException_Exception {
    }


    @After
    public void after() throws IOException, TimeoutException {
        bankManagement.stopChannelForTest();
    }


    @Then("the transfer is unsuccessful, since bank account does not exist")
    public void theTransferIsUnsuccessfulSinceBankAccountDoesNotExist() throws BankServiceException_Exception {
        bankManagement.handleTransferRequest();
        Assert.assertEquals("Transfer unsuccessful", bankManagement.getErrorMessage());
    }
}

