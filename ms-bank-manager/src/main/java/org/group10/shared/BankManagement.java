package org.group10.shared;

/** @author: Simon W. Ø. Larsen **/

import dtu.ws.fastmoney.*;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.concurrent.TimeoutException;


public class BankManagement {
    private Payment payment = new Payment();
    public static MQReceiver Qrecv;
    private static MQSender paymentSend;
    private static String message = "No new messages";
    private Boolean Error = false;
    private static String errorMessage ="true";
    BankService bank = new BankServiceService().getBankServicePort();

    /**
     * Bank manager waits for any TransferRequests to handle in his loop. The transferSuccess variable
     * shows the transfer status. The message gets reset in order to be ready to receive a new event.
     * Finally, a return message for the transfer success is handled.
     */
    public static void main(String[] args) throws Exception {
        BankManagement bankManagement = new BankManagement();
        bankManagement.startConnections();

        while(true) {

        message = Qrecv.receive();
        bankManagement.handleTransferRequest();
        bankManagement.handleReturnMessage(errorMessage);

        }

    }

    /**
     * @return RabbitMQ queue listener.
     */
    public static MQReceiver getQrecv() {
        return Qrecv;
    }


    /**
     * Start channel connection between the two RabbitMQ queues.
     * @throws Exception Unsuccessful rabbitMQ connection
     */
    public void startConnections() throws Exception {
        Qrecv = new MQReceiver(MQNaming.BankManager); //Payment from Bank
        paymentSend = new MQSender(MQNaming.PaymentManager); //Communicate with payment
        Qrecv.startChannel();

    }

    /**
     * When an event is received within the listener queue,
     * this.message acts as the event process status. Thus requires to
     * be reset an event-query.
     */

    public void setMessage(String message) {
        BankManagement.message = message;}

    /**
     * When user+merchant event has been received, the message is "peeled"
     * to retrieve the transfer information between the customer and the
     * merchant.
     */
    public void handleTransferRequest() throws BankServiceException_Exception {
        String customerCPR = ((new JSONObject(message)).getString("customerCPR"));
        String merchantCPR = ((new JSONObject(message)).getString("merchantCPR"));
        String amount = ((new JSONObject(message)).getString("amount"));
        if (!Error) {
        transfer_fast(customerCPR,merchantCPR,amount);}
    }




    /**
     * A response message - for the payment MS - describing the status of the transfer
     * is created through the PMJSON encoder and sent through the PM message queue.
     * @param transferSuccess If the transfer was a success or not
     * @throws Exception _
     */
    public void handleReturnMessage(String transferSuccess) throws Exception {
        String response;
        if (!Error){
            response = PaymentManagerJSONEncoder.setTransferStatus(transferSuccess, MQNaming.BankManager);
        }
        else {
            response = GeneralJSONEncoder.error(errorMessage);
        }
        paymentSend.send(response);
    }

    /**
     * Through the bank's interface, a transfer between the costumer and the merchant's
     * bank account - accessed with their CPR number - is made.
     * @param customerCPR Received from the PM
     * @param merchantCPR Received from the PM
     * @param amount Received from the PM
     */
    public void transfer_fast(String customerCPR, String merchantCPR, String amount) {
        try {
            String customerID = bank.getAccountByCprNumber(customerCPR).getId();
            String merchantID = bank.getAccountByCprNumber(merchantCPR).getId();

            bank.transferMoneyFromTo(customerID,
                    merchantID,
                    new BigDecimal(amount),
                    "A transfer of " + amount + " has been made.");


        } catch (Exception e){
            e.printStackTrace();
            System.out.println(Error);
            errorMessage = "Transfer unsuccessful";
            setError(true); }
    }

    /**
     * Returns the balance of a client from the bank with their CPR number.
     * @param CPR User's CPR number
     * @return Returns the balance of the client, if this client exists in the
     * bank system.
     */
    public BigDecimal getBalance(String CPR) throws BankServiceException_Exception {
        return bank.getAccountByCprNumber(CPR).getBalance();
    }

    /**
     * Returns ID of a client from the bank with their CPR number. Checks of someone has a bankaccount too.
     * @param CPR Bank user CPR number
     * @return String for bankID
     */
    public Boolean hasBankAccount(String CPR) throws BankServiceException_Exception {

        try {
            bank.getAccountByCprNumber(CPR);
            return true;
        }
        catch (Exception e) {
            return false;
        }
    }
    public Boolean getError() {
        return Error;
    }
    public void setError(Boolean bool) {
        this.Error = bool;
    }

    public String getErrorMessage() {
        return errorMessage;

    }

    /**
     * Stops receiver channels for CUCUMBER testing
     * @throws IOException _
     * @throws TimeoutException _
     */
    public void stopChannelForTest() throws IOException, TimeoutException {
        Qrecv.stopChannel();
    }

}
