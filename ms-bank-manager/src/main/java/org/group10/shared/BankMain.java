package org.group10.shared;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import io.vertx.ext.auth.User;

@QuarkusMain
public class BankMain implements QuarkusApplication {

    @Override
    public int run(String... args) throws Exception {
        System.out.println("Starting the BankManager ...");
        BankManagement bankManagement = new BankManagement();
        bankManagement.startConnections();
        MQReceiver Qrecv = bankManagement.getQrecv();



        while(true) {
            System.out.println("Waiting for a request ...");

            String message = Qrecv.receive();
            bankManagement.setMessage(message);
            bankManagement.handleTransferRequest();
            bankManagement.handleReturnMessage(bankManagement.getErrorMessage());
            System.out.println("Request handled: " + message);


        }
    }

}