package org.group10.shared;

/**
 * @author: Simon W. Ø. Larsen
 */

import dtu.ws.fastmoney.*;
import java.math.BigDecimal;

public class Payment {


    User costumerUser = new User();
    User merchantUser = new User();
    BankService bank = new BankServiceService().getBankServicePort();

    public Payment(){
    }

    public BigDecimal getCostumerBalance() throws BankServiceException_Exception {
        return bank.getAccountByCprNumber(costumerUser.getCprNumber()).getBalance();
    }

    public BigDecimal getMerchantBalance() throws BankServiceException_Exception {
        return bank.getAccountByCprNumber(merchantUser.getCprNumber()).getBalance();
    }

    public void setUserCostumer(String firstName, String lastName, String cprNumber){
        costumerUser.setFirstName(firstName);
        costumerUser.setLastName(lastName);
        costumerUser.setCprNumber(cprNumber);
    }

    public void setUserMerchant(String firstName, String lastName, String cprNumber){
        merchantUser.setFirstName(firstName);
        merchantUser.setLastName(lastName);
        merchantUser.setCprNumber(cprNumber);
    }

    public void createBankAccountCostumer(Integer int1) throws BankServiceException_Exception {
        try {
            bank.createAccountWithBalance(costumerUser, BigDecimal.valueOf(int1));
        } catch (BankServiceException_Exception e){
            System.out.println("Account already exists for costumer");
            bank.retireAccount(bank.getAccountByCprNumber(costumerUser.getCprNumber()).getId());
            bank.createAccountWithBalance(costumerUser, BigDecimal.valueOf(int1));
        }
    }

    public void createBankAccountMerchant(Integer int1) throws BankServiceException_Exception {
        try {
            bank.createAccountWithBalance(this.merchantUser, BigDecimal.valueOf(int1));
        } catch (BankServiceException_Exception e){
            System.out.println("Account already exists for merchant");
            bank.retireAccount(bank.getAccountByCprNumber(merchantUser.getCprNumber()).getId());
            bank.createAccountWithBalance(merchantUser, BigDecimal.valueOf(int1));
        }
    }

    public boolean transfer(String string) {
        try {
            bank.transferMoneyFromTo(bank.getAccountByCprNumber(costumerUser.getCprNumber()).getId(),
                    bank.getAccountByCprNumber(merchantUser.getCprNumber()).getId(),
                    new BigDecimal(string),
                    "A transfer of " + string + " has been made.");
            return true;
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }





}

