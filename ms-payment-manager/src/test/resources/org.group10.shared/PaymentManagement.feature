Feature: PaymentManagement
  Scenario: Sending a successful message to Payment
    Given a payment sender Q named PaymentManager
    And a payment receiver Q named PaymentManager
    And a Sender
    When we send a message with "abcd:userid1234", "merchantid1234", "10" and Sender
    Then the receiver Q should receive Json

  Scenario: Retrieve CPR from userManagement
    Given a payment sender Q named PaymentManager
    And a payment receiver Q named PaymentManager
    And a Sender
    And a user sender Q named UserManager
    When we send a message with "abcd:userid1234", "merchantid1234", "10" and Sender
    Then the receiver Q should receive Json
    Then we send a message to UserManager
    Then UserManager sends the costumer's CPR "12101-1234"

  Scenario: Costumer has insufficient funds
    Given a payment sender Q named PaymentManager
    And a payment receiver Q named PaymentManager
    And a Sender
    When we send a message with "abcd:userid1234", "merchantid1234", "3000" and Sender
    Then the payment is declared unsuccessful
