package org.group10.shared;
import org.group10.shared.*;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


public class PaymentManagementSteps {


    private static MQReceiver Qrecv;
    private static MQSender Qsend;
    private static MQSender QuserSender;
    private static MQReceiver QTransferRequest;
    private static MQReceiver QUserRecv;
    private static MQSender QUserPaymentSender;
    PaymentManagement payment = new PaymentManagement();
    MQNaming Sender;
    String message;
    String messageresponse;

    @Before()
    public void before() throws IOException, TimeoutException {
        Qrecv.stopChannel();
        QUserRecv.stopChannel();
    }
    @Given("a payment sender Q named PaymentManager")
    public void a_payment_sender_Q_named_PaymentManager()  {
        Qsend = new MQSender(MQNaming.PaymentManager);
        Qsend.setQueueName(MQNaming.PaymentManager);
    }


    @Given("a payment receiver Q named PaymentManager")
    public void a_payment_receiver_Q_named_PaymentManager() throws Exception {
        payment.startConnections();
        Qrecv = payment.getQrecv();

    }


    @Given("a Sender")
    public void a_Sender() {
        this.Sender = MQNaming.CustomerAPIResult;
    }


    @When("we send a message with {string}, {string}, {string} and Sender")
    public void we_send_a_message_with_info(String costumerInformation, String merchantID, String amount) throws Exception {
        // Act as the API is sending a transfer request
        PaymentManagerJSONEncoder JsonEncoder = new PaymentManagerJSONEncoder();
        this.message = JsonEncoder.createTransfer(costumerInformation, merchantID, amount, Sender);
        Qsend.send(message);
    }


    @Then("the receiver Q should receive Json")
    public void the_receiver_Q_should_receive() throws Exception {
        this.messageresponse = Qrecv.receive();
        payment.stopChannelForTest();
        Assert.assertEquals(message,messageresponse);
    }


    @Given("a user sender Q named UserManager")
    public void a_user_sender_Q_named_UserManager() {
        QuserSender = payment.getUserSend();
    }


    @Then("we send a message to UserManager")
    public void we_send_a_message_to_UserManager() throws Exception {
        payment.startConnections();
        payment.handleTransferRequest(messageresponse);

        TransferRequest transferRequest = payment.getTransferRequest();
        /* Order matters here !!! */
        String JsonRequestCostumer = UserManagerJSONEncoder.getUserCPR(transferRequest.getCustomerID(), MQNaming.PaymentResult);
        String JsonRequestMerchant = UserManagerJSONEncoder.getUserCPR(transferRequest.getMerchantID(), MQNaming.PaymentResult);
        QuserSender.send(JsonRequestCostumer);
        QuserSender.send(JsonRequestMerchant);
    }


    @Then("UserManager sends CPR {string} back")
    public void user_manager_sends_cpr_back(String string) throws Exception {
        QUserRecv.stopChannel();
        QUserRecv = new MQReceiver(MQNaming.UserManager);
        QUserRecv.startChannel();

        String paymentResponseCostumer = UserManagerJSONEncoder.getUserCPRResponse("userid1234", string, MQNaming.UserManager);
        QUserPaymentSender = new MQSender(MQNaming.PaymentResult);
        QUserPaymentSender.send(paymentResponseCostumer);
        Assert.assertTrue(true);
    }

    @Then("UserManager sends the costumer's CPR {string}")
    public void user_manager_sends_costumer_cpr(String string) throws Exception {

        String paymentResponseCostumer = UserManagerJSONEncoder.getUserCPRResponse("userid1234", string, MQNaming.UserManager);
        QUserPaymentSender = new MQSender(MQNaming.PaymentManager);
        QUserPaymentSender.send(paymentResponseCostumer);

        QTransferRequest = payment.getQrecv();


        String userCostumerResponse = QTransferRequest.receive();

        payment.stopChannelForTest();

        String costumerCPR = ((new JSONObject(userCostumerResponse)).getString("CPR"));
        Assert.assertEquals(string,costumerCPR);
    }


    @Then("the payment is declared unsuccessful")
    public void the_payment_is_unsuccessful(){

    }


    @After
    public void after() throws IOException, TimeoutException {
        payment.stopChannelForTest();
    }
}
