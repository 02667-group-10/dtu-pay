package org.group10.shared;
/**
 * @author: Oline Stærke
 */

public class TransferRequest {
    private String amount;
    private String merchantID;
    private MQNaming sender;
    private String customerToken;
    private String customerID;
    private String description;
    private String customerCPR;
    private String IDD;
    private String transferStatus;




    /**
     * TransferRequest is an object with all the needed information for any given transfer.
     */

    public TransferRequest() {

    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMerchantID() {
        return merchantID;
    }

    public void setMerchantID(String merchantID) {
        this.merchantID = merchantID;
    }

    public void setSender(String sender) {
        //this.sender = sender;
        this.sender = MQNaming.CustomerAPIResult;
    }

    public String getCustomerToken() {
        return customerToken;
    }

    public void setCustomerToken(String customerToken) {
        this.customerToken = customerToken;
    }

    public String getCustomerID() {
        return customerID;
    }

    public void setCustomerID(String customerID) {
        this.customerID = customerID;
    }

    public void setMerchantIDD(String IDD){this.IDD = IDD;}

    public String getMerchantIDD(){return this.IDD;}

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCustomerCPR() {
        return customerCPR;
    }

    public void setCustomerCPR(String customerCPR) {
        this.customerCPR = customerCPR;
    }



    public String getTransferStatus() {
        return transferStatus;
    }

    public void setTransferStatus(String transferStatus) {
        this.transferStatus = transferStatus;
    }






}
