package org.group10.shared;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class PaymentMain implements QuarkusApplication {

    @Override
    public int run(String... args) throws Exception {
        System.out.println("Starting the PaymentManager ...");
        PaymentManagement paymentManager = new PaymentManagement();
        paymentManager.startConnections();
        while (true) {
            System.out.println("Waiting for a request ...");
            String msg = paymentManager.recvMsg();
            System.out.println("Request handled: " + msg);
        }
    }

}
