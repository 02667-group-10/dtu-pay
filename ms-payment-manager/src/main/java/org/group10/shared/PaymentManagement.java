
/**
 * @author: Oline Stærke
 */

package org.group10.shared;

import org.json.JSONObject;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.concurrent.TimeoutException;

public class PaymentManagement {

    private static MQReceiver Qrecv;
    private static MQSender bankSend;
    private static MQSender userSend;
    private static TransferRequest transferRequest;
    private static String message = "No new messages";
    private static MQSender reportSend;
    private static MQSender APIsend;
    private Boolean transferError;




    /**
     * Bank manager waits for any TransferRequests to handle in his loop.
     * It sets up connections to the relevant messagesQs, so that it is ready to receive requests.
     * The message shows the current message being processed.
     * The message gets reset in order to be ready to receive a new event.
     * It handles getting the customer and merchant information.
     * It then sends a transfer request to the bank, and waits for a response.
     * If the response is successful, it send a log of the transfer to ReportManager.
     * Finally it then returns the response to CustomerAPI through a messageQ.
     */

    public static void main(String[] args) throws Exception {
        System.out.println("Starting the PaymentManager ...");
        PaymentManagement paymentManager = new PaymentManagement();
        paymentManager.startConnections();
        while (true) {
            System.out.println("Waiting for a request ...");
            String msg = paymentManager.recvMsg();
            System.out.println("Request handled: " + msg);
        }

    }


    public String recvMsg() throws Exception {

            message = Qrecv.receive(); // receive message
            handleTransferRequest(message); // Create transferRequest object
            if (checkCustomerInfo() && checkMerchantInfo()) {

                handleGetCustomerInfo(); // get customer CPR
                handleGetMerchantInfo(); // get merchant CPR
                if (!transferError) { // If they were no mistakes
                    handleBankTransfer();
                }// Send the request to bank
                if (!transferError) {// if transfer is successful, send response to Reportmanager.
                    handleReportLogs();
                    Thread.sleep(2000);
                    String transferSuccess = GeneralJSONEncoder.OKResponse(transferRequest.getMerchantID(), MQNaming.PaymentManager);
                    APIsend.send(transferSuccess);
                } else {
                    String transferSuccess = GeneralJSONEncoder.error(transferRequest.getTransferStatus(),
                            MQNaming.PaymentManager);
                    APIsend.send(transferSuccess);
                }
            }
            else {
                String error = GeneralJSONEncoder.error("User not found");
                APIsend.send(error);

        }
            return message;

    }

    public boolean checkCustomerInfo() throws Exception {

        String JsonRequest = UserManagerJSONEncoder.verifyUserRequest(transferRequest.getCustomerID(), MQNaming.PaymentManager);
        userSend.send(JsonRequest); //Send Request
        message = Qrecv.receive();
        if (new JSONObject(message).has("errorMessage")){
            return false;
        }
        return true;
    }

    public boolean checkMerchantInfo() throws Exception {

        String JsonRequest = UserManagerJSONEncoder.verifyUserRequest(transferRequest.getMerchantID(), MQNaming.PaymentManager);
        userSend.send(JsonRequest); //Send Request
        message = Qrecv.receive();
        System.out.println(message);
        if (new JSONObject(message).has("errorMessage")){
            return false;
        }
        return true;
    }


    /**
     * reset message
     */
    public void resetMessage() {
        message = "No new messages";
    }
    /**
     * @return transferRequest.
     */
    public static TransferRequest getTransferRequest() {
        return transferRequest;
    }
    /**
     * @return MQSender bankSend.
     */
    public static MQSender getBankSend() {
        return bankSend;
    }
    /**
     * @return MQSender userSend.
     */

    public static MQSender getUserSend() {
        return userSend;
    }
    /**
     * @return MQReceiver Qrecv.
     */
    public static MQReceiver getQrecv() {
        return Qrecv;
    }

    /**
     * @return MQSender APISend.
     */

    public static MQSender getAPIsend() {
        return APIsend;
    }



    /**
     * Start all connections to all the messageQs.
     */
    public void startConnections() throws Exception {
        Qrecv = new MQReceiver(MQNaming.PaymentManager); //Payment from RESTApi
        userSend = new MQSender(MQNaming.UserManager); //Communicate with Token manager
        bankSend = new MQSender(MQNaming.BankManager); //Communicate with Token sender
        reportSend = new MQSender(MQNaming.ReportManager); //Send logs to report
        APIsend = new MQSender(MQNaming.CustomerAPI); //Send information back to CustomerAPI
        Qrecv.startChannel(); //Starting the connection to rabbitMQ
    }

    /**
     * Create the transferRequest object from the transferRequest from CustomerAPI
     */

    public void handleTransferRequest(String message) {
        System.out.println(message);
        transferRequest = new TransferRequest(); //Create transfer request
        JSONObject jsonObject =new JSONObject(message);
        transferRequest.setAmount(jsonObject.getString("amount"));
        transferRequest.setMerchantID(jsonObject.getString("merchantID"));
        transferRequest.setMerchantIDD(jsonObject.getString("merchantID"));
        transferRequest.setSender(jsonObject.getString("sender"));
        transferRequest.setCustomerToken(jsonObject.getString("customerToken"));
        transferRequest.setCustomerID(jsonObject.getString("customerID"));
        transferRequest.setDescription(jsonObject.getString("description"));
    }

    /**
     * Handles the customer information needed to call the bank.
     * First it creates the JSONEncoder String in the right format.
     * Then it sends the request to Usermanager over the right messageQ.
     * Then it waits for the response.
     * Then it saves the CPR for later use.
     */

    public void handleGetCustomerInfo() throws Exception {
        //Send request to usermanager
        String JsonRequest = UserManagerJSONEncoder.getUserCPR(transferRequest.getCustomerID(), MQNaming.PaymentManager);
        userSend.send(JsonRequest); //Send Request
        message = Qrecv.receive();

        //Wait for response on our own Q

        System.out.println(message);
        /* Error Handling */
        handleError(message);
        String customerCPR = ((new JSONObject(message)).getString("CPR"));
        /*                */


        transferRequest.setCustomerCPR(customerCPR);



    }

    /**
     * Handles the merchant information needed to call the bank.
     * First it creates the JSONEncoder String in the right format.
     * Then it sends the request to Usermanager over the right messageQ.
     * Then it waits for the response.
     * Then it saves the CPR for later use.
     */
    public void handleGetMerchantInfo() throws Exception {
        String JsonRequest = UserManagerJSONEncoder.getUserCPR(transferRequest.getMerchantID(), MQNaming.PaymentManager);
        userSend.send(JsonRequest); //Send Request
        message = Qrecv.receive();
        handleError(message);
        String merchantCPR = ((new JSONObject(message)).getString("CPR"));


        transferRequest.setMerchantID(merchantCPR);




    }
    /**
     * Handles the bank transfer.
     * Send the request to bank.
     * Handles any error in the bank response event.
     */
    public void handleBankTransfer() throws Exception {
        //Create request
        String JsonRequest = BankManagerJSONEncoder.transferBank(
                "transferBank",
                transferRequest.getCustomerCPR(),
                transferRequest.getMerchantID(),
                transferRequest.getAmount(),
                MQNaming.PaymentManager);
        bankSend.send(JsonRequest); // Send message


        message = Qrecv.receive(); // Wait for response
        System.out.println(message);
        /* Error Handling */
        handleError(message);
    }

    /**
     * Handles the report logs.
     * First it creates the JSONEncoder String in the right format.
     * Then it sends the request to ReportManager.
     */

    public void handleReportLogs() throws Exception {
        //Create formatted string
        String log = ReportManagerJSONEncoder.sendTransactionInfo(new Timestamp(System.currentTimeMillis()).toString(),
                transferRequest.getCustomerID(), transferRequest.getMerchantIDD(), transferRequest.getAmount(),
                transferRequest.getCustomerToken());
        reportSend.send(log); //Send information
    }

    public void handleError(String message){
        this.transferError = ((new JSONObject(message)).has("errorMessage"));
        if (transferError) {
            transferRequest.setTransferStatus((new JSONObject(message)).getString("errorMessage"));
        }
    }

    /**
     * Stops receiver channels for CUCUMBER testing
     * @throws IOException _
     * @throws TimeoutException _
     */
    public void stopChannelForTest() throws IOException, TimeoutException {
        Qrecv.stopChannel();
    }
}
