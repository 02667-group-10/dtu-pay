package org.group10.shared;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.JSONObject;

import java.sql.Timestamp;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author David Fager
 */
public class RequestHandlingAndReportingSteps {

    ReportManager reportManager = new ReportManager(true);
    JSONObject fabricatedTransaction;
    JSONObject response;

    public RequestHandlingAndReportingSteps() throws Exception {
    }

    @Given("a transaction request with timestamp: {string}, customer ID: {string}, merchant ID: {string}, money: {string}, token: {string}")
    public void a_transaction_request_with_timestamp_customer_id_merchant_id_money_token(String timeStamp, String customerID, String merchantID, String money, String token) {
        fabricatedTransaction = new JSONObject(ReportManagerJSONEncoder.sendTransactionInfo(timeStamp, customerID, merchantID, money, token));
    }

    @Given("the transaction is handled")
    public void the_transaction_is_handled() throws Exception {
        reportManager.handleMsg(fabricatedTransaction);
    }

    @When("a complete report is requested with sender: {string}")
    public void a_complete_report_is_requested_with_sender(String sender) throws Exception {
        response = new JSONObject(reportManager.handleMsg(new JSONObject(ReportManagerJSONEncoder.getCompleteReportRequest(sender))));
    }

    @Then("the report response consists of reportType: {string}, the transaction and sender: {string}")
    public void the_report_response_consists_of_report_type_the_transaction_and_sender(String reportType, String sender) {
        assertEquals(reportType, response.getString("document"));
        assertEquals(fabricatedTransaction.toString(), response.getJSONArray("report").get(0).toString());
        assertEquals(sender, response.getString("sender"));
    }

    @When("a customer report is requested with customerID: {string}, fromTime: {string}, untilTime: {string} and sender: {string}")
    public void a_customer_report_is_requested_with_customer_id_from_time_to_time_and_sender(String customerID, String fromTime, String untilTime, String sender) throws Exception {
        response = new JSONObject(reportManager.handleMsg(new JSONObject(ReportManagerJSONEncoder.getCustomerReportRequest(customerID, Timestamp.valueOf(fromTime), Timestamp.valueOf(untilTime), MQNaming.CustomerAPI))));
    }

    @When("a merchant report is requested with merchantID: {string}, fromTime: {string}, untilTime: {string} and sender: {string}")
    public void a_merchant_report_is_requested_with_merchant_id_from_time_to_time_and_sender(String merchantID, String fromTime, String untilTime, String sender) throws Exception {
        response = new JSONObject(reportManager.handleMsg(new JSONObject(ReportManagerJSONEncoder.getMerchantReportRequest(merchantID, Timestamp.valueOf(fromTime), Timestamp.valueOf(untilTime), MQNaming.CustomerAPI))));
        fabricatedTransaction.remove("customerID");
    }

}