package org.group10.shared;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

/**
 * @author David Fager
 */
public class SimpleTransactionLoggingSteps {

    ReportManager reportManager = new ReportManager(true);
    JSONObject fabricatedTransaction;
    ArrayList<JSONObject> report;

    public SimpleTransactionLoggingSteps() throws Exception {
    }

    @Given("a simple transaction with timestamp: {string}, customer ID: {string}, merchant ID: {string}, money: {string}, token: {string}")
    public void a_simple_transaction_with_timestamp_customer_id_merchant_id_money_token(String timeStamp, String customerID, String merchantID, String money, String token) {
        fabricatedTransaction = new JSONObject(ReportManagerJSONEncoder.sendTransactionInfo(timeStamp, customerID, merchantID, money, token));
    }

    @When("the transaction is logged for the system")
    public void the_transaction_is_logged_for_the_system() {
        reportManager.logForSystem(fabricatedTransaction);
    }

    @Then("the system log consists of the transaction")
    public void the_system_log_consists_of_the_transaction() {
        report = reportManager.getCompleteReport();
        assertEquals(1, report.size());
        assertEquals(fabricatedTransaction, report.get(0));
    }

    @When("the transaction is logged for the customer")
    public void the_transaction_is_logged_for_the_customer() {
        reportManager.logForUser(ReportManager.userType.CUSTOMER, fabricatedTransaction);
    }

    @Then("the customer log consists of the transaction")
    public void the_customer_log_consists_of_the_transaction() {
        report = reportManager.getUserReport(ReportManager.userType.CUSTOMER, fabricatedTransaction.getString("customerID"),
                new Timestamp(0), new Timestamp(System.currentTimeMillis()));
        assertEquals(1, report.size());
        assertEquals(fabricatedTransaction, report.get(0));
    }

    @When("the transaction is logged for the merchant")
    public void the_transaction_is_logged_for_the_merchant() {
        reportManager.logForUser(ReportManager.userType.MERCHANT, fabricatedTransaction);
    }

    @Then("the merchant log consists of the transaction")
    public void the_merchant_log_consists_of_the_transaction() {
        report = reportManager.getUserReport(ReportManager.userType.MERCHANT, fabricatedTransaction.getString("merchantID"),
                new Timestamp(0), new Timestamp(System.currentTimeMillis()));
        assertEquals(1, report.size());
        assertEquals(fabricatedTransaction, report.get(0));
    }

}