Feature: handling and reporting transactions

  Scenario: complete report looks correct after one transaction is handled
    Given a transaction request with timestamp: "2021-01-03 03:03:03.003", customer ID: "CID1", merchant ID: "MID1", money: "3000", token: "CID1:tteesstt-tteesstt"
    And the transaction is handled
    When a complete report is requested with sender: "UserManager"
    Then the report response consists of reportType: "completeReport", the transaction and sender: "ReportManager"

  Scenario: customer report looks correct after one transaction is handled
    Given a transaction request with timestamp: "2021-01-13 13:13:13.013", customer ID: "CID2", merchant ID: "MID2", money: "13000", token: "CID12:tteesstt-tteesstt"
    And the transaction is handled
    When a customer report is requested with customerID: "CID2", fromTime: "1970-01-01 01:00:00.0", untilTime: "2021-01-30 00:30:30.03" and sender: "PaymentManager"
    Then the report response consists of reportType: "customerReport", the transaction and sender: "ReportManager"

  Scenario: merchant report looks correct after one transaction is handled
    Given a transaction request with timestamp: "2021-01-23 23:23:23.023", customer ID: "CID3", merchant ID: "MID3", money: "23000", token: "CID3:tteesstt-tteesstt"
    And the transaction is handled
    When a merchant report is requested with merchantID: "MID3", fromTime: "1970-01-01 01:00:00.0", untilTime: "2021-01-31 00:31:31.031" and sender: "BankManager"
    Then the report response consists of reportType: "merchantReport", the transaction and sender: "ReportManager"
