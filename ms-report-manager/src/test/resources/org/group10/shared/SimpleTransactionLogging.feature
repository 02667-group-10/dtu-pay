Feature: simple transaction logging

  Scenario: a transaction is logged for the system
    Given a simple transaction with timestamp: "2021-01-05 05:05:05.005", customer ID: "CID1", merchant ID: "MID1", money: "5000", token: "CID1:test-test-test"
    When the transaction is logged for the system
    Then the system log consists of the transaction

  Scenario: a transaction is logged for the customer
    Given a simple transaction with timestamp: "2021-01-10 10:10:10.01", customer ID: "CID2", merchant ID: "MID2", money: "10000", token: "CID2:test-test-test"
    When the transaction is logged for the customer
    Then the customer log consists of the transaction

  Scenario: a transaction is logged for the merchant
    Given a simple transaction with timestamp: "2021-01-15 15:15:15.015", customer ID: "CID3", merchant ID: "MID3", money: "15000", token: "CID3:test-test-test"
    When the transaction is logged for the merchant
    Then the merchant log consists of the transaction
