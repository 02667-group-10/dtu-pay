package org.group10.shared;

import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * @author David Fager
 * This class saves 'logs' (any transactions made by the PaymentManager) as JSON objects.
 * It also generates a 'report', which is either all transactions ever made or a specific
 * customer or merchant's transactions in a given time period.
 */
public class ReportManager {

    private boolean testMode;
    private MQReceiver mqReceiver;
    private MQSender mqSender;
    private ArrayList<JSONObject> allTransactions;
    private HashMap<String, ArrayList<JSONObject>> sortedTransactionsCustomer;
    private HashMap<String, ArrayList<JSONObject>> sortedTransactionsMerchant;

    public enum userType {CUSTOMER, MERCHANT}

    /**
     * Is run when instantiating a new ReportManager object.
     * It prepares the resources, including opening the in/out message queues and the arraylists and hashmaps.
     *
     * @throws Exception in case of error in the receiver.
     */

    public static void main(String[] args) throws Exception {
        System.out.println("Starting the ReportManager ...");

        ReportManager reportManager = new ReportManager(false);
        while (true) {
            System.out.println("Waiting for a request ...");
            String msg = reportManager.recvMsg();
            System.out.println("Request handled: " + msg);
        }

    }
    public ReportManager(boolean testMode) throws Exception {
        this.testMode = testMode;

        // Don't start connections if testMode is true
        if (!testMode) {
            mqReceiver = new MQReceiver(MQNaming.ReportManager);
            mqReceiver.startChannel();

            mqSender = new MQSender(MQNaming.CustomerAPI); //Init to nothing, as it is set later
        }

        allTransactions = new ArrayList<>();
        sortedTransactionsCustomer = new HashMap<>();
        sortedTransactionsMerchant = new HashMap<>();
    }

    /**
     * Blocks on the .receive() call, and send any received message to the handleMsg() method.
     *
     * @return the message that was received on the message queue receiver.
     * @throws Exception if either the mqReceiver throws one or the handleMsg() does.
     */
    public String recvMsg() throws Exception {
        String receivedMessage = mqReceiver.receive();
        System.out.println(receivedMessage);
        handleMsg(new JSONObject(receivedMessage));
        return receivedMessage;
    }

    /**
     * This method directs a request to the right method, by looking at the JSON object's request "key" value.
     * Is the request to log a transaction, then nothing is returned. Is the request to get a report, then the
     * right report method is called, with the right arguments.
     *
     * @param receivedJSON is the message received by the message queue receiver.
     * @return the response 'pack' (JSON object) that contains information depending on the received request.
     * @throws Exception
     */
    public String handleMsg(JSONObject receivedJSON) throws Exception {
        ArrayList<JSONObject> report;
        String pack = "";

        if (receivedJSON.has("request")) {
            switch (receivedJSON.getString("request")) {
                case "logTransaction":
                    logForSystem(receivedJSON); // Logs the transaction sorted by timestamp
                    logForUser(userType.CUSTOMER, receivedJSON); // Logs the transaction sorted by customer
                    logForUser(userType.MERCHANT, receivedJSON); // Logs the transaction sorted by merchant
                    break;
                case "getCompleteReport":
                    report = getCompleteReport();

                    pack = ReportManagerJSONEncoder.encodeReportResponse(
                            "completeReport", report, MQNaming.ReportManager);
                    break;
                case "getCustomerReport":
                    report = getUserReport(userType.CUSTOMER,
                            receivedJSON.getString("customerID"),
                            Timestamp.valueOf(receivedJSON.getString("fromTime")),
                            Timestamp.valueOf(receivedJSON.getString("untilTime")));

                    pack = ReportManagerJSONEncoder.encodeReportResponse(
                            "customerReport", report, MQNaming.ReportManager);
                    break;
                case "getMerchantReport":
                    report = getUserReport(userType.MERCHANT,
                            receivedJSON.getString("merchantID"),
                            Timestamp.valueOf(receivedJSON.getString("fromTime")),
                            Timestamp.valueOf(receivedJSON.getString("untilTime")));

                    pack = ReportManagerJSONEncoder.encodeReportResponse(
                            "merchantReport", report, MQNaming.ReportManager);
                    break;
                default:
                    pack = ReportManagerJSONEncoder.error("Bad Request", MQNaming.ReportManager.toString());
                    break;
            }
        } else {
            pack = ReportManagerJSONEncoder.error("Bad Request", MQNaming.ReportManager.toString());
        }

        // Send the packed JSON object, if one was created (all but 'logTransaction' should send a packet back)
        if (!pack.isEmpty() && !testMode) {
            sendMsg(receivedJSON.getString("sender"), pack);
        }

        return pack;
    }

    /**
     * Sends a message to the message queue of the microservice that requested a report.
     *
     * @param sendTo determines who the recipient of the message/pack (json) is.
     * @param pack   is the message/pack payload, which is a JSON object translated to a String.
     * @throws Exception if the message queue sender should encounter one.
     */
    public void sendMsg(String sendTo, String pack) throws Exception {
        for (MQNaming name : MQNaming.values()) {
            if (MQNaming.valueOf(sendTo).equals(name)) {
                // The sendTo name was recognized
                mqSender.setQueueName(MQNaming.valueOf(sendTo));
                mqSender.send(pack);
                return;
            }
        }

        // The sendTo name must not have been recognized, so send an error to whoever requested
        mqSender.setQueueName(MQNaming.valueOf(sendTo));
        mqSender.send(ReportManagerJSONEncoder.error("Unknown sender", MQNaming.ReportManager.toString()));
    }

    /**
     * Logs the received transaction in the total system log ArrayList
     *
     * @param receivedJSON contains the received transaction, in JSON format.
     */
    public void logForSystem(JSONObject receivedJSON) {
        allTransactions.add(receivedJSON);
        System.out.println(allTransactions);
    }

    /**
     * Logs a received transaction for either the customer or merchant. If for the merchant, then the customer ID is removed.
     * The logging is done by simply adding the transaction JSON object received, to an ArrayList of JSON objects.
     *
     * @param userType     is an enum that tells whether to save the transaction in the customer or merchant hashmap.
     * @param receivedJSON contains the received transaction, in JSON format.
     */
    public void logForUser(userType userType, JSONObject receivedJSON) {
        HashMap<String, ArrayList<JSONObject>> transactionList = null;
        String IDKey = "";

        if (userType.equals(ReportManager.userType.CUSTOMER)) {
            transactionList = sortedTransactionsCustomer;
            IDKey = "customerID";
        } else if (userType.equals(ReportManager.userType.MERCHANT)) {
            transactionList = sortedTransactionsMerchant;
            IDKey = "merchantID";

            // Censor the customer ID for the merchant
            if (receivedJSON.has("customerID"))
                receivedJSON.remove("customerID");
        }

        // Adding the transaction to the customer sorted list of transactions
        if (transactionList.containsKey(receivedJSON.getString(IDKey))) {
            transactionList.get(receivedJSON.getString(IDKey)).add(receivedJSON);
        } else {
            ArrayList<JSONObject> transaction = new ArrayList<>();
            transaction.add(receivedJSON);
            transactionList.put(receivedJSON.getString(IDKey), transaction);
        }
    }

    public ArrayList<JSONObject> getCompleteReport() {
        return allTransactions;
    }

    /**
     * Sends back the report of the user, either customer or merchant, by looking for the given user ID in the
     * respective hashmap. When the right hashmap entry is found, then the ArrayList of transaction JSON objects is
     * checked, and each transaction that is after the provided "fromTime"-stamp and before the "toTime"-stamp is returned.
     *
     * @param userType  is an enum determining if it is the customer or merchant that needs to be reported on.
     * @param userID    is the ID of either the customer or merchant, to lookup in the respective hashmap.
     * @param fromTime  is the start of the time period for transactions needing to be reported. (closest to epoch)
     * @param untilTime is the end of the time period for transactions needing to be reported. (farthest from epoch)
     * @return the ArrayList of transactions in JSON objects.
     */
    public ArrayList<JSONObject> getUserReport(userType userType, String userID, Timestamp fromTime, Timestamp untilTime) {
        ArrayList<JSONObject> transactionList = null;
        if (userType.equals(ReportManager.userType.CUSTOMER)) {
            transactionList = sortedTransactionsCustomer.get(userID);
        } else if (userType.equals(ReportManager.userType.MERCHANT)) {
            transactionList = sortedTransactionsMerchant.get(userID);
        }

        ArrayList<JSONObject> report = new ArrayList<>();
        for (JSONObject json : transactionList) {
            Timestamp timestamp = Timestamp.valueOf(json.getString("timestamp"));
            if (timestamp.after(fromTime) && timestamp.before(untilTime)) {
                report.add(json);
            }
        }

        return report;
    }

}
