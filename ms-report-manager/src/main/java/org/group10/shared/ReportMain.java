package org.group10.shared;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;

@QuarkusMain
public class ReportMain implements QuarkusApplication {

    @Override
    public int run(String... args) throws Exception {
        System.out.println("Starting the ReportManager ...");

        ReportManager reportManager = new ReportManager(false);
        while (true) {
            System.out.println("Waiting for a request ...");
            String msg = reportManager.recvMsg();
            System.out.println("Request handled: " + msg);
        }
    }

}
