package org.group10.shared;

//Cannot run in shared context. Move to a MS project in order to run.

public class Test {

    public static void main(String[] argv) throws Exception {
            //Sender setup
            MQSender sender_payment = new MQSender(MQNaming.UserManager);

            //Receiver setup
            MQReceiver receiver_payment = new MQReceiver(MQNaming.UserManager);
            receiver_payment.startChannel();

            //Sending and receiving Hello 5 times.
        for (int i = 0; i < 5; i++) {

            //Send Hello
            sender_payment.send("HELLO round:"+Integer.toString(i));

            //Receive Hello
            String message = receiver_payment.receive();
            System.out.println(" [x] Received '" + message + "'");
            String message2 = receiver_payment.receive();
            System.out.println(" [x] Received '" + message2 + "'");

        }
    }
}
