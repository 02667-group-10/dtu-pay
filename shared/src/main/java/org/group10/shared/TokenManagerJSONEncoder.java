package org.group10.shared;



import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class TokenManagerJSONEncoder {
    private static JSONObject default_package(String ID, MQNaming Sender){
        JSONObject myObject = new JSONObject();
        myObject.put("ID", ID);
        myObject.put("sender", Sender.toString());

        return myObject;
    }


    // ************ REQUEST encodings ************************************ //


    public static String generateTokensRequest(String id, int numRequestedTokens, MQNaming sender){
        JSONObject myObject = default_package(id, sender);
        myObject.put("numRequestedTokens", String.valueOf(numRequestedTokens));
        myObject.put("request", "tokenRequest");

        return myObject.toString();
    }
    // ************ RESPONSE encodings ************************************ //

    // Not used - using "OKResponse" from generalJSONEncodings instead
    public static String generateTokensResponse(String id, MQNaming sender) {
        JSONObject myObject = default_package(id, sender);
        myObject.put("response", "response generateTokensResponse");
        myObject.put("tokensSend", "true");

        return myObject.toString();
    }






    public static String error(String errorMessage, MQNaming sender) {
    JSONObject myObject = new JSONObject();

    myObject.put("errorMessage", errorMessage);
    myObject.put("sender", sender);


    return myObject.toString();
    }

}
