
/**
 * @author: Oline Stærke
 */
package org.group10.shared;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class MQReceiver {

    public MQReceiver(MQNaming name) {
        this.setQueueName(name);
    }

    public MQNaming QUEUE_NAME; //Defines which Q we are handling
    public Channel channel;
    public String message = "No new messages";
    public Connection connection;



    public void setQueueName(MQNaming QueueName) {
        this.QUEUE_NAME = QueueName;
    }
    public void resetMessage() {
        this.message = "No new messages";
    }

    public MQNaming getQueueName() {
        return QUEUE_NAME;
    }

    public void startChannel() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        boolean testing = false;

        if (testing) {factory.setHost("localhost");}
        else {factory.setHost("rabbitMq");
            factory.setPort(5672);//<-- for local machine testing. DO NOT PUSH if you've changed this!
        }
        this.connection = factory.newConnection();
        this.channel = connection.createChannel();


        channel.queueDeclare(QUEUE_NAME.toString(), false, false, false, null);

    }


    public String receive() throws IOException, InterruptedException, TimeoutException {
        String current_message = "No new messages";
        while (message == "No new messages") {

                Thread.sleep(1000); //To ensure that we do not check the queue constantly. Maybe there is a better way to do this.


                DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                    this.message = new String(delivery.getBody(), "UTF-8");
                };
                channel.basicConsume(QUEUE_NAME.toString(), true, deliverCallback, consumerTag -> {

                });

                current_message = message;
            }
        System.out.println(message + "...");

        resetMessage();


        return current_message;
    }

    public void stopChannel() throws IOException, TimeoutException {
        channel.close();

    }
}




