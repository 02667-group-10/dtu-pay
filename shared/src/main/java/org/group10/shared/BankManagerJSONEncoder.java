package org.group10.shared;
/**
 * @author: Oline Stærke
 */


import org.json.JSONObject;

public class BankManagerJSONEncoder {

    public static String transferBank(String description, String customerCPR, String merchantCPR, String amount, MQNaming Sender) {
        JSONObject myObject = new JSONObject();
        myObject.put("merchantCPR", merchantCPR);
        myObject.put("customerCPR", customerCPR);

        myObject.put("amount", amount);
        myObject.put("description", description);
        myObject.put("sender", Sender.toString());
        return myObject.toString();

    }
}
