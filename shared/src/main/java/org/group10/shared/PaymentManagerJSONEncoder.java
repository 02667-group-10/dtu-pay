package org.group10.shared;

/**
 * @author: Simon W Ø Larsen
 */

import org.json.JSONObject;

public class PaymentManagerJSONEncoder {
    //CostumerInformation = UUID from token
    public static String createTransfer(String costumerInformation,String merchantID,String amount,MQNaming Sender) {
        String[] costumerInfo = costumerInformation.split(":");
        String costumerID = costumerInfo[0];
        String costumerToken = costumerInfo[1];
        JSONObject myObject = new JSONObject();
        myObject.put("description","createTransfer");
        myObject.put("customerToken", costumerToken);
        myObject.put("customerID", costumerID);
        myObject.put("merchantID", merchantID);
        myObject.put("amount", amount);
        myObject.put("sender", Sender.toString());

        return myObject.toString();

    }

    public static String setTransferStatus(String transferSuccess, MQNaming Sender) {
        // String "true" eller "false"
        // Should we make a transfer object? Or just keep it as a boolean object?
        JSONObject myObject = new JSONObject();

        myObject.put("transfer", transferSuccess);
        myObject.put("request", "verify");
        myObject.put("sender", Sender.toString());

        return myObject.toString();

    }


}
