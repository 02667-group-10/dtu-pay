package org.group10.shared;

/**
 * @author: Mathias Søndergaard
 */

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class UserManagerJSONEncoder {
    /**
     * Default package which packs ID and sender.
     * @param ID ID of user.
     * @param Sender Sender.
     * @return Returns the default package, as a JSON String
     */
    private static JSONObject default_package(String ID, MQNaming Sender){
        JSONObject myObject = new JSONObject();
        myObject.put("ID", ID);
        myObject.put("sender", Sender.toString());

        return myObject;
    }


    // ************ REQUEST encodings ************************************ //
    /**
     * Delete package.
     * @return delete package which contains the request to delete, as JSON String
     */
    public static String deleteUserRequest(String ID, MQNaming Sender) {
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "delete");

        return myObject.toString();
    }

    /**
     * @return verify user package which contains the request to verify, as JSON String
     */
    public static String verifyUserRequest(String ID, MQNaming Sender) {
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "verify");

        return myObject.toString();
    }

    /**
     * @param CPR CPR of client (user)
     * @param customer if true, creates the user as a customer. If false, creates the user as merchant.
     * @param name Name of client
     * @param surname Surname of client
     * @return create user request.
     */
    public static String createUserRequest(String ID, String CPR, Boolean customer, String name, String surname, MQNaming Sender) {
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "create");
        myObject.put("CPR", CPR);
        myObject.put("customer", customer);
        myObject.put("name", name);
        myObject.put("surname",surname);

        return myObject.toString();
    }


    /**
     * @return CPR request
     */
    public static String getUserCPR(String ID, MQNaming Sender){
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "getCPR");

        return myObject.toString();
    }

    /**
     * @return UserName request
     */
    public static String getUserName(String ID, MQNaming Sender){
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "getName");

        return myObject.toString();
    }

    /**
     * @return UserName request
     */
    public static String getUserSurName(String ID, MQNaming Sender){
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "getSurname");

        return myObject.toString();
    }





    // ********************* RESPONSE encodings ************************************ //

    /**
     * @param cpr cpr of a client
     * @return response package.
     */
    public static String getUserCPRResponse(String ID, String cpr,MQNaming Sender){
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "response getUserCPR");
        myObject.put("CPR", cpr);

        return myObject.toString();
    }



    /**
     * @param name name of a client
     * @return response package.
     */
    public static String getUserNameResponse(String ID, String name,MQNaming Sender){
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "response getUserName");
        myObject.put("name", name);

        return myObject.toString();
    }


    /**
     * @param surname surname of a client
     * @return response package.
     */
    public static String getUserSurNameResponse(String ID, String surname, MQNaming Sender){
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("request", "response getUserSurname");
        myObject.put("surname", surname);

        return myObject.toString();
    }

    /**
     * @param verify true if user exists, otherwise false.
     * @return response package.
     */
    public static String verifyUserResponse(String ID, Boolean verify, MQNaming Sender) {
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("verify", verify.toString());
        myObject.put("request", "response verifyUser");

        return myObject.toString();
    }



    // ********************* DEPRECATED encodings ************************************ //


    /**
     * @deprecated DO NOT USE - ALL TOKEN STUFF IS IN TM NOW!
     * @param amountOfTokens the amount of tokens a customer has
     * @return the corresponding package which should be sent.
     */
    public static String getAmountOfTokensResponse(String ID, int amountOfTokens, MQNaming Sender){
        throw new java.lang.RuntimeException("deprecated code used. Aborting.");
        //JSONObject myObject = default_package(ID, Sender);
        //myObject.put("amountOfTokens", String.valueOf(amountOfTokens));
        //myObject.put("request", "response getAmountOfTokens");

        //return myObject.toString();
    }

    /**
     * @deprecated DO NOT USE - ALL TOKEN STUFF IS IN TM NOW!
     * @param token string which represents a token, which has been fetched from a customer
     * @return response package, containing token.
     */
    public static String getTokenResponse(String ID, String token,MQNaming Sender){
        throw new java.lang.RuntimeException("deprecated code used. Aborting.");

        //JSONObject myObject = default_package(ID, Sender);
        //myObject.put("request", "response getToken");
        //myObject.put("token", token);

        //return myObject.toString();
    }

    /**
     * @deprecated DO NOT USE - ALL TOKEN STUFF IS IN TM NOW!
     * @return token request
     */
    public static String getTokenRequest(String ID, MQNaming Sender){
        throw new java.lang.RuntimeException("deprecated code used. Aborting.");

        //JSONObject myObject = default_package(ID, Sender);
        //myObject.put("request", "getToken");

        //return myObject.toString();
    }

    /**
     * @deprecated DO NOT USE - ALL TOKEN STUFF IS IN TM NOW!
     * @param tokens ArrayList of tokens
     * @return This request should be used by TM to add generated tokens for customer.
     */
    public static String addTokensRequest(String ID, MQNaming Sender, ArrayList<String> tokens) {
        throw new java.lang.RuntimeException("deprecated code used. Aborting.");


        //JSONObject myObject = default_package(ID, Sender);
        //myObject.put("tokens", new JSONArray(tokens));
        //myObject.put("request", "addTokens");

        //return myObject.toString();
    }

    /**
     * @deprecated DO NOT USE - ALL TOKEN STUFF IS IN TM NOW!
     * @return token amount request.
     */
    public static String getTokenAmountRequest(String ID, MQNaming Sender) {
        throw new java.lang.RuntimeException("deprecated code used. Aborting.");

        //JSONObject myObject = default_package(ID, Sender);
        //myObject.put("request", "getTokenAmount");

        //return myObject.toString();
    }

}
