package org.group10.shared;


import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.util.ArrayList;

/**
 * @author David Fager
 * This class standardizes the communication between the report manager microservice, and any other microservice.
 */
public class ReportManagerJSONEncoder {

    // Request methods

    /**
     * Sends a JSON object translated into a string, containing the needed information to correctly log a transaction.
     *
     * @param timestamp        of the moment the transaction was successfully made.
     * @param customerID       the ID of the customer, that accepted the transaction.
     * @param merchantID       the ID of the merchant, that started the transaction.
     * @param moneyTransferred the amount of money that was sent in the transaction.
     * @param token            the unique token that the customer sent/used with the transaction.
     * @return the JSON object with the information, translated into a string.
     */
    public static String sendTransactionInfo(String timestamp, String customerID, String merchantID, String moneyTransferred, String token) {
        return new JSONObject()
                .put("request", "logTransaction")
                .put("timestamp", timestamp)
                .put("customerID", customerID)
                .put("merchantID", merchantID)
                .put("moneyTransferred", moneyTransferred)
                .put("token", token).toString();
    }

    /**
     * Determines the information needed for the Report Manager to send the complete report of the transactions in the system.
     *
     * @param sender the (queue)name of the microservice that sent the request.
     * @return the JSON object of the request, translated into a string.
     */
    public static String getCompleteReportRequest(String sender) {
        return new JSONObject()
                .put("request", "getCompleteReport")
                .put("sender", sender).toString();
    }

    /**
     * Determines the information needed for the Report Manager to send the customer's report of the transactions in the system.
     *
     * @param customerID the ID of the customer, that has requested the report.
     * @param fromTime   the start time of the period that the customer wants to view transactions for. (closest to epoch)
     * @param untilTime  the end time of the period that the customer wants to view transactions for. (farthest from epoch)
     * @param sender     the (queue)name of the microservice that sent the request.
     * @return the JSON object of the request, translated into a string.
     */
    public static String getCustomerReportRequest(String customerID, Timestamp fromTime, Timestamp untilTime, MQNaming sender) {
        return new JSONObject()
                .put("request", "getCustomerReport")
                .put("customerID", customerID)
                .put("fromTime", fromTime)
                .put("untilTime", untilTime)
                .put("sender", sender).toString();
    }

    /**
     * Determines the information needed for the Report Manager to send the merchant's report of the transactions in the system.
     *
     * @param merchantID the ID of the merchant, that requested the report.
     * @param fromTime   the start time of the period that the merchant wants to view transactions for. (closest to epoch)
     * @param untilTime  the end time of the period that the merchant wants to view transactions for. (farthest from epoch)
     * @param sender     the (queue)name of the microservice that sent the request.
     * @return the JSON object of the request, translated into a string.
     */
    public static String getMerchantReportRequest(String merchantID, Timestamp fromTime, Timestamp untilTime, MQNaming sender) {
        return new JSONObject()
                .put("request", "getMerchantReport")
                .put("merchantID", merchantID)
                .put("fromTime", fromTime)
                .put("untilTime", untilTime)
                .put("sender", sender).toString();
    }


    // Response methods

    /**
     * Determines how the answer looks, when the Report Manager sends back its response to the microservice that requested a report.
     * The report is given as an ArrayList but repacked to a JSONArray.
     *
     * @param reportType is the type of report that is returned. Could be completeReport, customerReport or merchantReport.
     * @param report     is the ArrayList of the JSON objects that make up the report/transactions.
     * @param sender     the (queue)name of the Report Manager.
     * @return the JSON object of the response, translated into a string.
     */
    public static String encodeReportResponse(String reportType, ArrayList<JSONObject> report, MQNaming sender) {
        return new JSONObject()
                .put("document", reportType)
                .put("report", new JSONArray(report))
                .put("sender", sender.toString()).toString();
    }

    /**
     * This method determines how an error looks, when a request results in one.
     *
     * @param errorMessage is the string message describing what error happened.
     * @param sender       the (queue)name of the Report Manager.
     * @return the JSON object of the error, translated into a string.
     */
    public static String error(String errorMessage, String sender) {
        return new JSONObject()
                .put("errorMessage", errorMessage)
                .put("sender", sender).toString();
    }

}
