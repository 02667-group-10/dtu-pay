package org.group10.shared;

/**
 * @author David Fager
 */
public enum MQNaming {
    UserManager,
    BankManager,
    PaymentManager,
    PaymentResult,
    ReportManager,
    TokenManager,
    CustomerAPI,
    MerchantAPI,
    CustomerAPIResult
}
