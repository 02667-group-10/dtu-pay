package org.group10.shared;

/**
 * @author: Oline Stærke
 */

//NOTE : RABBITMQ has to run on another server. Right now this is working when it is running on local host.
// I Am using Brew services start rabbitmq

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.nio.charset.StandardCharsets;

public class MQSender {
    public MQNaming QUEUE_NAME; //Defines which Q we are handling

    public MQSender(MQNaming name) {
        this.QUEUE_NAME = name;
    }

    public void setQueueName(MQNaming QueueName) {
        this.QUEUE_NAME = QueueName;
    }

    public MQNaming getQueueName() {
        return QUEUE_NAME;
    }

    public void send(String message) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        boolean testing = false;

        if (testing) {factory.setHost("localhost");}
        else {factory.setHost("rabbitMq");
            factory.setPort(5672);//<-- for local machine testing. DO NOT PUSH if you've changed this!
           }

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME.toString(), false, false, false, null);
            channel.basicPublish("", QUEUE_NAME.toString(), null, message.getBytes(StandardCharsets.UTF_8));
            System.out.println(" [x] Sent '" + message + "'");

        }
    }
}