package org.group10.shared;

/**
 * @author: Mathias Søndergaard
 */

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class GeneralJSONEncoder {

    /**
     * @param ID ID
     * @param Sender Who sent the message
     * @return a default package each other package should extend on.
     */
    private static JSONObject default_package(String ID, MQNaming Sender){
        JSONObject myObject = new JSONObject();
        myObject.put("ID", ID);
        myObject.put("sender", Sender.toString());

        return myObject;
    }

    /**
     * @param errorMessage A string which explains the error / context of error
     * @return An error message, formattet as JSON String
     */
    // ********************** RESPONSE **************************** //
    // Overload on error, to handle all types of errors
    public static String error(String errorMessage, MQNaming sender, String ID) {
        JSONObject myObject = default_package(ID, sender);
        myObject.put("errorMessage", errorMessage);

        return myObject.toString();
    }

    /**
     * Overload on error JSONEncoding.
     */
    public static String error(String errorMessage, MQNaming sender) {
        JSONObject myObject = new JSONObject();

        myObject.put("errorMessage", errorMessage);
        myObject.put("sender", sender.toString());


        return myObject.toString();
    }

    /**
     * Overload on error JSONEncoding.
     */
    public static String error(String errorMessage, String ID) {
        JSONObject myObject = new JSONObject();

        myObject.put("errorMessage", errorMessage);
        myObject.put("ID", ID);

        return myObject.toString();
    }

    /**
     * Overload on error JSONEncoding.
     */
    public static String error(String errorMessage) {
        JSONObject myObject = new JSONObject();

        myObject.put("errorMessage", errorMessage);

        return myObject.toString();
    }

    /**
     * Specified error message for handling user not being found (typically used in UM context)
     */
    public static String userNotFoundError(String errorMessage, MQNaming sender, String ID) {
        JSONObject myObject = default_package(ID, sender);
        myObject.put("errorMessage", "User not found - " + errorMessage);

        return myObject.toString();
    }

    /**
     * @param ID ID
     * @param sender sender
     * @return generic OK Response
     */
    public static String OKResponse(String ID, MQNaming sender){
        JSONObject myObject = default_package(ID, sender);
        myObject.put("response", "OK");

        return myObject.toString();

    }

    /**
     * @param ID ID
     * @param sender sender
     * @param OKResponseMSG specified OK response as string
     * @return Specified OK response
     */
    public static String OKResponse(String ID, MQNaming sender, String OKResponseMSG){
        JSONObject myObject = default_package(ID, sender);
        myObject.put("response", "OK " + OKResponseMSG);

        return myObject.toString();

    }


    public static String TokensRequest(String ID, MQNaming Sender, ArrayList<String> tokens) {
        JSONObject myObject = default_package(ID, Sender);
        myObject.put("tokens", new JSONArray(tokens));
        myObject.put("request", "addTokens");

        return myObject.toString();
    }
}
