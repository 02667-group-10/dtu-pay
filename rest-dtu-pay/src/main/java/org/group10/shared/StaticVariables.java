/**
 * @author: Simon James Jensen
 */
package org.group10.shared;

/**
 * Since we're not using persistence we just use a static int to advance id numbers across user types
 */
public class StaticVariables {

    public static int id = 0;


}
