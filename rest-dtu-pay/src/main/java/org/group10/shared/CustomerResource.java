/**
 * @author: Simon James Jensen
 */
package org.group10.shared;

import org.json.JSONArray;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@Path("/customers")
public class CustomerResource {

    private MQSender tokenSender;
    private MQSender reportSender;
    private MQSender userSender;
    private MQReceiver receiver;

    private boolean connected = true;

    /**
     * Constructor for CustomerAPI. Connects to relevant queues.
     */
    public CustomerResource() {
        try {
            tokenSender = new MQSender(MQNaming.TokenManager);
            userSender = new MQSender(MQNaming.UserManager);
            receiver = new MQReceiver(MQNaming.CustomerAPI);
            reportSender = new MQSender(MQNaming.ReportManager);


        } catch (Exception e) {
            e.printStackTrace();
            connected = false;
        }
    }

    /**
     * Uses a default ID for the customer. The ID is incremented for each client that is created.
     *
     * @param firstName String firstname of customer
     * @param lastName  String lastname of customer
     * @param cpr       String cpr of customer
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createCustomer(
            @QueryParam("firstName") String firstName,
            @QueryParam("lastName") String lastName,
            @QueryParam("cpr") String cpr) {
        if (!connected) {
            return serviceUnavailable();
        }

        //TODO : Increment ID
        String userRequest = UserManagerJSONEncoder.createUserRequest(Integer.toString(++StaticVariables.id), cpr, true, firstName, lastName, MQNaming.CustomerAPI);
        return (getResponse(userRequest, userSender));


    }

    /**
     * @param customerid String customer id
     * @return String customer id
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{customerId}")
    public Response deleteCustomer(
            @PathParam("customerId") String customerid) {
        if (!connected) {
            return serviceUnavailable();
        }
        String userRequest = UserManagerJSONEncoder.deleteUserRequest(customerid,
                MQNaming.CustomerAPI);
        return getResponse(userRequest, userSender);
    }


    /**
     * A method that gets the token (which is a string)
     *
     * @param customerId     String ID of customer
     * @param amountOfTokens int. How many tokens that the customer wants.
     * @return
     */
    @GET
    @Path("/{customerId}/tokens")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTokens(
            @PathParam("customerId") String customerId,
            @QueryParam("amountOfTokens") int amountOfTokens
    ) {
        if (!connected) {
            return serviceUnavailable();
        }
        try {
            String tokensRequest = TokenManagerJSONEncoder.generateTokensRequest(customerId, amountOfTokens, MQNaming.CustomerAPI);
            return (getResponse(tokensRequest, tokenSender));

        } catch (Exception e) {
            return serviceUnavailable();
        }
    }

    /**
     * Starts the reciver channel
     */
    private void init() throws Exception {
        receiver.startChannel();
    }

    /**
     * @return Generic response for service not available
     */
    private Response serviceUnavailable() {
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
    }


    private Response getResponse(String createTransfer, MQSender sender) {
        try {
            init();
            System.out.println(createTransfer);
            sender.send(createTransfer);
            String jsonMessage = receiver.receive();
            receiver.stopChannel();
            JSONObject json = new JSONObject(jsonMessage);
            if (json.has("document")) {
                ArrayList<String> report = extractJSONReport(json.getJSONArray("report"));
                return Response.ok(report).build();

            } else if (json.has("response")) {
                return Response
                        .ok(json.getString("ID"), MediaType.APPLICATION_JSON)
                        .build();

            } else if (json.has("tokens")) {
                ArrayList<String> token = extractJSONArray(json.getJSONArray("tokens"));
                return Response.ok(token).build();

            } else if (json.has("errorMessage")) {
                return Response
                        .status(Response.Status.BAD_REQUEST)
                        .entity(json.get("errorMessage"))
                        .build();

            }

        } catch (Exception e) {
            return serviceUnavailable();
        }
        return serviceUnavailable();
    }

    /**
     * @param ar JSONArray array of JSON
     * @return
     */
    private ArrayList<String> extractJSONArray(JSONArray ar) {
        ArrayList<String> tokensList = new ArrayList<>();

        if (ar != null) {
            for (int i = 0; i < ar.length(); i++) {
                tokensList.add(ar.getString(i));
            }
        }
        return tokensList;
    }

    private ArrayList<String> extractJSONReport(JSONArray ar) {
        ArrayList<String> tokensList = new ArrayList<>();
        if (ar != null) {
            for (int i = 0; i < ar.length(); i++) {
                JSONObject jsonObj = ar.getJSONObject(i);
                tokensList.add(jsonObj.getString("request"));
                tokensList.add(jsonObj.getString("moneyTransferred"));
                tokensList.add(jsonObj.getString("merchantID"));
                tokensList.add(jsonObj.getString("timestamp"));
                tokensList.add(jsonObj.getString("token"));
            }
        }
        return tokensList;
    }
    /**
     * @param customerId String customer Id
     * @param from       String from date
     * @param to         String to date
     * @return Response
     */
    @GET
    @Path("/{customerId}/customerReport")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCustomerReport(
            @PathParam("customerId") String customerId,
            @QueryParam("from") String from,
            @QueryParam("to") String to

    ) {
        if (!connected) {
            return serviceUnavailable();
        }
        try {
            Timestamp timestamp_from = Timestamp.valueOf(from);
            Timestamp timestamp_to = Timestamp.valueOf(to);

            String reportRequest = ReportManagerJSONEncoder.getCustomerReportRequest(customerId, timestamp_from, timestamp_to, MQNaming.CustomerAPI);
            return(getResponse(reportRequest,reportSender));

        } catch (Exception e) {
            return serviceUnavailable();
        }
    }
}