package org.group10.shared;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

// @Author: Simon James Jensen
@Path("/merchants")
public class MerchantResource {

    private MQSender paymentSender;
    private MQSender userSender;
    private MQSender reportSender;

    private MQReceiver receiver;

    private boolean connected = true;

    public MerchantResource() {

        try {
            paymentSender = new MQSender(MQNaming.PaymentManager);
            userSender = new MQSender(MQNaming.UserManager);
            receiver = new MQReceiver(MQNaming.CustomerAPI);
            reportSender = new MQSender(MQNaming.ReportManager);
        } catch (Exception e) {
            connected = false;
        }
    }

    /**
     * @param firstName String first name
     * @param lastName  String last name
     * @param cpr       String cpr
     * @return Response response object
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createMerchant(
            @QueryParam("firstName") String firstName,
            @QueryParam("lastName") String lastName,
            @QueryParam("cpr") String cpr) {

        if (!connected) {
            return serviceUnavailable();
        }
        String userRequest = UserManagerJSONEncoder.createUserRequest(
                Integer.toString(++StaticVariables.id),
                cpr,
                false,
                firstName,
                lastName,
                MQNaming.CustomerAPI
        );
        return getResponse(userRequest, userSender);
    }

    /**
     * @param merchantId String customer id
     * @return String customer id
     */
    @DELETE
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{merchantId}")
    public Response deleteMerchant(
            @PathParam("merchantId") String merchantId) {
        if (!connected) {
            return serviceUnavailable();
        }
        String userRequest = UserManagerJSONEncoder.deleteUserRequest(merchantId,
                MQNaming.CustomerAPI);
        return getResponse(userRequest, userSender);
    }

    /**
     * @param merchantId String merchant Id
     * @param token      String customer token
     * @param amount     String amount of money
     * @return
     */
    @POST
    @Path("/{merchantId}/payment")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTransfer(
            @PathParam("merchantId") String merchantId,
            @QueryParam("token") String token,
            @QueryParam("amount") String amount) {
        if (!connected) {
            return serviceUnavailable();
        }
        String createTransfer = PaymentManagerJSONEncoder.createTransfer(token, merchantId, amount, MQNaming.MerchantAPI);
        return getResponse(createTransfer, paymentSender);
    }

    private Response getResponse(String createTransfer, MQSender sender) {
        try {
            init();
            sender.send(createTransfer);
            String jsonMessage = receiver.receive();
            receiver.stopChannel();
            JSONObject json = new JSONObject(jsonMessage);

            if (json.has("response")) {
                return Response
                        .ok(json.getString("ID"), MediaType.APPLICATION_JSON)
                        .build();

            } else if (json.has("errorMessage")) {
                return Response
                        .status(Response.Status.BAD_REQUEST)
                        .entity(json.get("errorMessage"))
                        .build();
            }
            else if (json.has("document")) {
                ArrayList<String> report = extractJSONReport(json.getJSONArray("report"));
                return Response.ok(report).build();
            }

        } catch (Exception e) {
            return serviceUnavailable();
        }
        return serviceUnavailable();
    }

    private void init() throws Exception {
        receiver.startChannel();
    }

    private Response serviceUnavailable() {
        return Response.status(Response.Status.SERVICE_UNAVAILABLE).build();
    }

    /**
     * @param merchantId String merchant id
     * @param from       String from date
     * @param to         String to date
     * @return Response
     */
    @GET
    @Path("/{merchantId}/merchantReport")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getMerchantReport(
            @PathParam("merchantId") String merchantId,
            @QueryParam("from") String from,
            @QueryParam("to") String to

    ) {
        if (!connected) {
            return serviceUnavailable();
        }
        try {
            // String looks like yyyy-MM-dd hh:mm:ss.SSS
            Timestamp timestamp_from = Timestamp.valueOf(from);
            Timestamp timestamp_to = Timestamp.valueOf(to);

            String reportRequest = ReportManagerJSONEncoder.getMerchantReportRequest(merchantId, timestamp_from, timestamp_to, MQNaming.CustomerAPI);
            return(getResponse(reportRequest,reportSender));
            //return null;
        } catch (Exception e) {
            return serviceUnavailable();
        }
    }
    private ArrayList<String> extractJSONReport(JSONArray ar) {
        ArrayList<String> tokensList = new ArrayList<>();
        if (ar != null) {
            for (int i = 0; i < ar.length(); i++) {
                JSONObject jsonObj = ar.getJSONObject(i);
                tokensList.add(jsonObj.getString("request"));
                tokensList.add(jsonObj.getString("moneyTransferred"));
                tokensList.add(jsonObj.getString("merchantID"));
                tokensList.add(jsonObj.getString("timestamp"));
                tokensList.add(jsonObj.getString("token"));
            }
        }
        return tokensList;
    }
}