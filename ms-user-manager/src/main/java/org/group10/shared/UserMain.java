package org.group10.shared;

import io.quarkus.runtime.QuarkusApplication;
import io.quarkus.runtime.annotations.QuarkusMain;
import io.vertx.ext.auth.User;

@QuarkusMain
public class UserMain implements QuarkusApplication {

    @Override
    public int run(String... args) throws Exception {
        System.out.println("Starting the UserManager ...");

        UserManager UM = new UserManager(false);
        while (true) {
            System.out.println("Waiting for a request ...");
            String msg = UM.recvMsg();
            System.out.println("Request handled: " + msg);
        }
    }

}
