package org.group10.shared;

/**
 * @author: Mathias Søndergaard
 */
public class Client {
    public String ID;
    public String CPR;
    public String name;
    public String surname;

    public Client(String ID, String CPR, String name, String surname) {
        this.ID = ID;
        this.CPR = CPR;
        this.name = name;
        this.surname = surname;
    }

    @Override
    public String toString() {
        return "cpr: " + CPR + " ID: " + ID + " name: " + name + " surname: " + surname;
    }

    @Override
    public boolean equals(Object Client) {
        Client C = (Client) Client;

        return ((this.ID.equals(C.ID)) && (this.surname.equals(C.surname)) &&
                (this.name.equals(C.name)) && (this.CPR.equals(C.CPR)));
    }

}
