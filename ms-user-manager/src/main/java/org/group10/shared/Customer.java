package org.group10.shared;

/**
 * @author: Mathias Søndergaard
 * Customer class that exists in scope of userManager
 */
public class Customer extends Client {

    public Customer(String ID, String CPR, String name, String surname) {
        super(ID, CPR, name, surname);
    }

}

