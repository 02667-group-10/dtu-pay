package org.group10.shared;

/**
 * @author: Mathias Søndergaard
 * Class that handles everything relevant to userManager MicroService
 */
import java.util.HashMap;
import java.util.Map;

import org.json.JSONObject;

public class UserManager {
    public Map<String, Customer> customers;
    public Map<String, Merchant> merchants;
    private MQReceiver qRecv;
    private MQSender tokenSend;
    private MQSender customerSend;
    private MQSender merchantSend;
    private MQSender paymentSend;
    private Boolean isTesting;

    private String id;
    private String sender;
    private String request;


    /**
     * Must be inside own function, such that we can test without having connections.
     */
    public void startConnections() throws Exception {
        qRecv = new MQReceiver(MQNaming.UserManager);
        tokenSend = new MQSender(MQNaming.TokenManager);
        customerSend = new MQSender(MQNaming.CustomerAPI);
        merchantSend = new MQSender(MQNaming.MerchantAPI);
        paymentSend = new MQSender(MQNaming.PaymentManager);
        qRecv.startChannel();
    }

    /**
     * Constructor which is always used in local testing context.
     */
    public UserManager()  {
        customers = new HashMap<>();
        merchants = new HashMap<>();
        isTesting = true;

    }

    /**
     * Constructor that can be used in end-2-end testing context.
     * @param testing True: UserManager has all communcation to requests/responds disabled.
     */
    public UserManager(Boolean testing) throws Exception {
        this();
        if (!testing) {
            isTesting = false;
            this.startConnections();
        }
    }

    /**
     * @param ID ID of client
     * @return surname if it exists
     */
    public String getSurname(String ID){
        Client C = getClient(ID);
        if (C == null)
            return null;
        return C.surname;
    }

    /**
     * @param ID ID of client
     * @return name if it exists
     */
    public String getName(String ID){
        Client C = getClient(ID);
        if (C == null)
            return null;
        return C.name;
    }

    /**
     * @param ID ID of client
     * @return CPR if it exists
     */
    public String getCPR(String ID){
        Client C = getClient(ID);
        if (C == null)
            return null;
        return C.CPR;
    }



    /**
     * @param ID ID of client
     * @return Client, if it exists
     */
    public Client getClient(String ID){
        if (customers.containsKey(ID))
            return customers.get(ID);
        if (merchants.containsKey(ID))
            return merchants.get(ID);
        return null;
    }

    /**
     * ID's are unique, so a shortcircuit "or" is used.
     * @param ID of customer/merchant
     * @return bool - if customer/merchant exists.
     */
    public boolean verifyClient(String ID){
        return (customers.containsKey(ID) || merchants.containsKey(ID));
    }


    /**
     * @param surname surname of client
     * @param name name of client
     * @param CPR  CPR of client
     * @param ID ID of client (customer or merchant)
     * @param Customer true if the wanted client has to be a customer. False for merchant
     */
    public String createClient(String ID, String CPR, boolean Customer, String name, String surname){
        try {
            if (Customer) {
                this.customers.put(ID, new Customer(ID, CPR, name, surname));
            } else {
                this.merchants.put(ID, new Merchant(ID, CPR, name, surname));
            }
            return GeneralJSONEncoder.OKResponse(id, MQNaming.UserManager, request);
        }
        catch (Exception E){
            return GeneralJSONEncoder.error("User already created", MQNaming.UserManager, ID);
        }
    }

    /**
     * @param ID ID of client (customer or merchant)
     */
    public void deleteClient(String ID){
        if (this.getClient(ID) instanceof Customer)
            customers.remove(ID);
        else
            merchants.remove(ID);
    }




    /**
     * A method for checking the validity of a message. If message is invalid, automatically sends a coresponding -
     * error message
     * @param msgObject The message received.
     * @return True if the msg is valid, otherwise false
     */
    private Boolean isMSGValid(JSONObject msgObject) throws Exception {
        String pack;
        sender = "";
        // To be succesful there must be request, sender and ID.
        if (msgObject.has("request") && (msgObject.has("sender")) && (msgObject.has("ID"))){
            request = msgObject.getString("request");
            sender = msgObject.getString("sender");
            id = msgObject.getString("ID");

            // Request "create" obviously cannot be verified.
            if (!(request.equals("create"))){
                if (verifyClient(id))
                    return true;
                else{
                    pack = GeneralJSONEncoder.userNotFoundError(request, MQNaming.UserManager, id);
                    sendMsg(pack, sender);
                    return false;
                }
            }

            return true;
        }
        else{
            if (msgObject.has("sender")){
                sender = msgObject.getString("sender");
            }
            // NO sender? Will be handled in sendMsg
            pack = GeneralJSONEncoder.error("Params request, sender or/and ID is missing", MQNaming.UserManager);
            sendMsg(pack, sender);
            return false;
        }
    }

    /**
     * The first layer of handling a message. Receives it and passes it to the handleMsg method.
     * @return The return statement is used only for debugging purposes.
     */
    public String recvMsg() throws Exception {
        String msg = qRecv.receive();
        handleMsg(new JSONObject(msg));
        return msg;

    }

    /**
     * The main logic for handling messages. Uses a switch to match request.
     * Some error handling has been implemented to catch the most common exceptions/errors.
     * @param msgObject JSONObject of the message
     * @return The return statement is used only for debugging/testing purposes.
     */
    public String handleMsg(JSONObject msgObject) throws Exception {
        String pack = "";
        if (isMSGValid(msgObject)) {
            try {
                switch (request) {
                    case "create":
                        pack = this.createClient(id, msgObject.getString("CPR"),
                                msgObject.getBoolean("customer"), msgObject.getString("name"),
                                msgObject.getString("surname"));
                        break;

                    case "delete":
                        this.deleteClient(id);
                        pack = GeneralJSONEncoder.OKResponse(id, MQNaming.UserManager, request);
                        break;

                    case "verify":
                        pack = UserManagerJSONEncoder.verifyUserResponse(id, this.verifyClient(id), MQNaming.UserManager);
                        break;


                    case "getCPR":
                        pack = UserManagerJSONEncoder.getUserCPRResponse(id, getCPR(id), MQNaming.UserManager);
                        break;

                    case "getName":
                        pack = UserManagerJSONEncoder.getUserNameResponse(id, getName(id), MQNaming.UserManager);
                        break;

                    case "getSurname":
                        pack = UserManagerJSONEncoder.getUserSurNameResponse(id, getSurname(id), MQNaming.UserManager);
                        break;

                    default:
                        pack = GeneralJSONEncoder.error("Request not understood", MQNaming.UserManager, id);
                }
            }
            // Bad practice: However errors being caught here are only internal errors
            catch(Error E){
                pack = GeneralJSONEncoder.error("Internal error", MQNaming.UserManager, id); }

            sendMsg(pack, sender);
        }
        // This is for "isTesting" - respond with a generic error msg
        else{
            pack = GeneralJSONEncoder.error("Generic error 4 testing", MQNaming.UserManager, id);
        }

        if (isTesting)
            return pack;
        return null;

    }

    /**
     * Class for sending messages.
     * Switch does not work for enums, so use "ififif..." :
     * Does not work due to: Constant expression required -  compile does not know enum is constant.
     * @param pack JSONEncoded package
     * @param to The receiver of pack
     */
    public void sendMsg(String pack, String to) throws Exception {
        if (!isTesting) {
            if (to.equals(MQNaming.TokenManager.toString())) {
                tokenSend.send(pack);
                return;
            }
            if (to.equals(MQNaming.CustomerAPI.toString())) {
                customerSend.send(pack);
                return;

            }
            if (to.equals(MQNaming.MerchantAPI.toString())) {
                merchantSend.send(pack);
                return;

            }
            if (to.equals(MQNaming.PaymentManager.toString())) {
                paymentSend.send(pack);
                return;

            }
            throw new java.lang.RuntimeException("No connection for sending. Might have received a MSG from a strange MS? To: " + to);
        }


    }

    /**
     * Main for end-2-end testing. Uses UserManager(false) to reflect this.
     */
    public static void main(String[] args) throws Exception {
        UserManager UM = new UserManager(false);

        while(true) {
            String message = UM.recvMsg();
            System.out.println(message);

        }

    }

}
