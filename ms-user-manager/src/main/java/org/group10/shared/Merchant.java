package org.group10.shared;

/**
 * @author: Mathias Søndergaard
 * Merchant class that exists in scope of userManager
 */
public class Merchant extends Client {

    public Merchant(String ID, String CPR, String name, String surname){
        super(ID, CPR, name, surname);
    }

}
