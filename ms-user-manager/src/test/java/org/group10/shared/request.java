package org.group10.shared;
/**
 * @author: Mathias Søndergaard
 */
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.cucumber.java.hu.De;
import org.group10.shared.Customer;
import org.group10.shared.MQNaming;
import org.group10.shared.UserManager;
import org.json.JSONObject;

import java.util.ArrayList;

import static org.group10.shared.UserManagerJSONEncoder.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class request {
    UserManager UM = new UserManager(true);
    String req;
    Customer C;
    String ID;

    public request() throws Exception {
    }

    @Given("ID: {string}: CPR: {string}: Name: {string} and Surname: {string} as JSON string")
    public void id_cpr_name_and_surname_as_json_string(String ID, String CPR, String name, String surname) {
        this.ID = ID;
        req = createUserRequest(ID, CPR,true, name, surname, MQNaming.CustomerAPI);
        C = new Customer(ID, CPR, name, surname);
    }
    @When("Creating the customer as a request")
    public void creating_the_customer_as_a_request() throws Exception {
        UM.handleMsg(new JSONObject(req));

    }
    @Then("a Customer is created through request handler")
    public void a_customer_is_created_through_request_handler() {

        assertEquals(C, UM.getClient(ID));

    }
    @Then("the customer is verified by a request")
    public void the_customer_is_verified_by_a_request() throws Exception {
        String verifyReq = verifyUserRequest(ID,  MQNaming.CustomerAPI);
        String verifyResponse = UM.handleMsg(new JSONObject(verifyReq));
        Boolean verify = Boolean.valueOf(new JSONObject(verifyResponse).getString("verify"));
        assertTrue(verify);
    }
    @Then("CPR should be gotten by a request")
    public void cpr_should_be_gotten_by_a_request() throws Exception {

        String CPRRequest = getUserCPR(ID, MQNaming.CustomerAPI);
        String CPRResponse = UM.handleMsg(new JSONObject(CPRRequest));

        String CPR = (new JSONObject(CPRResponse).getString("CPR"));


        assertEquals(CPR, UM.getCPR(ID));


    }

    String badRequest;
    String errorMsg;
    @Given("request BadRequest")
    public void request_bad_request() {
        JSONObject myObject = new JSONObject();
        myObject.put("ID", "Stupid sender");
        myObject.put("sender", "this is a bad bad sender");
        myObject.put("request", "invaild request");
        badRequest = myObject.toString();

    }
    @When("UserManagement tries to handle it")
    public void user_management_tries_to_handle_it() throws Exception {
        errorMsg = UM.handleMsg(new JSONObject(badRequest));
    }
    @Then("An error response is returned")
    public void an_error_response_is_returned() {
        assertTrue((new JSONObject(errorMsg)).has("errorMessage"));
    }

    @Given("ID: {string}: CPR: {string}: Name: {string} and Surname: {string} as customer")
    public void id_cpr_name_and_surname_as_customer(String ID, String CPR, String name, String surname) throws Exception {
        this.id_cpr_name_and_surname_as_json_string(ID, CPR, name, surname);
        this.creating_the_customer_as_a_request();
    }


    @When("deleting customer with id {string} as a request")
    public void deleting_customer_with_id_as_a_request(String string) throws Exception {
        String DeleteRequest = deleteUserRequest(string, MQNaming.CustomerAPI);

        UM.handleMsg(new JSONObject(DeleteRequest));

    }


    @When("Sending request with sender = {string} and no request key")
    public void sending_request_with_sender_and_no_request_key(String string) {
        JSONObject myObject = new JSONObject();
        myObject.put("sender", string);
        badRequest = myObject.toString();
    }

    @Then("Name, surname should be gotten by a request")
    public void name_surname_should_be_gotten_by_a_request() throws Exception {

        String NameRequest = getUserName(ID, MQNaming.CustomerAPI);
        String NameResponse = UM.handleMsg(new JSONObject(NameRequest));
        String name = (new JSONObject(NameResponse).getString("name"));

        String surr = getUserSurName(ID, MQNaming.CustomerAPI);
        String surR = UM.handleMsg(new JSONObject(surr));
        String sur = (new JSONObject(surR).getString("surname"));


        assertEquals(name, UM.getName(ID));
        assertEquals(sur, UM.getSurname(ID));

    }







}
