package org.group10.shared;
/**
 * @author: Mathias Søndergaard
 */
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.group10.shared.Customer;
import org.group10.shared.Merchant;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class simpleClient {
    UserManager UM = new UserManager(true);
    String ID;
    String CPR;
    String name;
    String surname;
    Customer C;
    Merchant M;

    public simpleClient() throws Exception {
    }

    @Given("ID: {string}: CPR: {string}: Name: {string} and Surname: {string}")
    public void id_cpr_name_and_surname(String ID, String CPR, String name, String surname) {
        this.ID = ID;
        this.CPR = CPR;
        this.name = name;
        this.surname = surname;
    }


    @When("Creating the customer")
    public void creating_the_customer() {
        C = new Customer(ID, CPR, name, surname);
        UM.createClient(ID, CPR, true, name, surname);
        System.out.println(C.toString());
    }

    @Then("a Customer is created")
    public void a_customer_is_created() {

        assertEquals(C, UM.getClient(ID));
        assertEquals(C.CPR, UM.getCPR(ID));
        assertEquals(C.name, UM.getName(ID));
        assertEquals(C.surname, UM.getSurname(ID));


    }

    @When("deleting customer with ID {string}")
    public void deleting_customer_with_id(String ID) {
        UM.deleteClient(ID);
    }

    @Then("No customers are present")
    public void no_customers_are_present() {
        assertEquals(0, UM.customers.size());
    }

    @When("Creating the merchant")
    public void creating_the_merchant() {
        M = new Merchant(ID, CPR, name, surname);
        UM.createClient(ID, CPR, false, name, surname);
    }
    @Then("a merchant is created")
    public void a_merchant_is_created() {
        assertEquals(M, UM.getClient(ID));

    }

    @When("deleting merchant with ID {string}")
    public void deleting_merchant_with_id(String ID) {
        UM.deleteClient(ID);
    }
    @Then("No merchants are present")
    public void no_merchants_are_present() {
        assertEquals(0, UM.merchants.size());

    }



}