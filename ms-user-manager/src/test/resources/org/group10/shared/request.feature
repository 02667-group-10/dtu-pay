## Auth: Mathias Søndergaard

Feature: Available requests
  Scenario: verify and create request
    Given ID: "01": CPR: "232323-1234": Name: "Bob" and Surname: "Hansen" as JSON string
    When Creating the customer as a request
    Then a Customer is created through request handler
    And the customer is verified by a request
    And CPR should be gotten by a request
    And Name, surname should be gotten by a request

  Scenario: Bad request
    Given request BadRequest
    When UserManagement tries to handle it
    Then An error response is returned


  Scenario: Delete a customer
    Given ID: "01": CPR: "232323-1234": Name: "Bob" and Surname: "Hansen" as customer
    When deleting customer with id "01" as a request
    Then No customers are present


  Scenario: A very bad request
    Given ID: "01": CPR: "232323-1234": Name: "Bob" and Surname: "Hansen" as customer
    When Sending request with sender = "very bad sender" and no request key
    And UserManagement tries to handle it
    Then An error response is returned

