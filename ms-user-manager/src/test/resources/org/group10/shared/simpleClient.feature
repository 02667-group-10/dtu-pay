## Auth: Mathias Søndergaard

Feature: User management
  Scenario: Create a Customer
    Given ID: "01": CPR: "232323-1234": Name: "Bob" and Surname: "Hansen"
    When Creating the customer
    Then a Customer is created

  Scenario: Delete a Customer
    Given ID: "01": CPR: "232323-1234": Name: "Bob" and Surname: "Hansen"
    When deleting customer with ID "01"
    Then No customers are present

  Scenario: Create a Merchant
    Given ID: "01": CPR: "232323-1234": Name: "Bob" and Surname: "Hansen"
    When Creating the merchant
    Then a merchant is created

  Scenario: Delete a Merchant
    Given ID: "01": CPR: "232323-1234": Name: "Bob" and Surname: "Hansen"
    When deleting merchant with ID "01"
    Then No merchants are present


