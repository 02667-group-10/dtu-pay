package org.group10.local.clients;

/**
 * @author: Simon James Jensen
 */

import org.group10.local.models.Customer;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;

/**
 * org.group10.local.clients.CustomerClient functions as the client for calls customer-path of the rest API
 * It builds a new client and routes function calls to the relevant rest API path.
 * Entities are built from arguments and depending on the nature of the response object data is either returned
 * or Exceptions are passed back to be handled by the instantiating class.
 */

/**
 * org.group10.local.clients.CustomerClient functions as the client for calls customer-path of the rest API
 * It builds a new client and routes function calls to the relevant rest API path.
 * Entities are built from arguments and depending on the nature of the response object data is either returned
 * or Exceptions are passed back to be handled by the instantiating class.
 */
public class CustomerClient {
    WebTarget baseUrl;

    public CustomerClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = (WebTarget) client.target("http://localhost:8007"); //to connect from local client to server: http://g-10.compute.dtu.dk:8007
    }

    /**
     *
     * @param customer  Customer object
     * @return          String customer ID
     * @throws Exception
     */
    public String createCustomer(Customer customer) throws Exception {
        Response response = baseUrl
                .path("/customers")
                .queryParam("firstName", customer.getFirstName())
                .queryParam("lastName", customer.getLastName())
                .queryParam("cpr", customer.getCpr())
                .request(MediaType.APPLICATION_JSON)
                .post(null);
        if (response.getStatus() == 200) {
            return response.readEntity(String.class);
        } else {
            String message = response.readEntity(String.class);
            throw new Exception(message);
        }
    }

    /**
     *
     * @param customerId        String customer ID
     * @param amountOfTokens    int amount of tokens requested
     * @return                  List<String> tokens
     * @throws Exception
     */
    public List<String> getTokens(String customerId, int amountOfTokens) throws Exception {
        Response response = baseUrl
                .path("/customers/" + customerId + "/tokens")
                .queryParam("amountOfTokens", amountOfTokens)
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {

            return response.readEntity(new GenericType<List<String>>() {
            });
        } else {
            String message = response.readEntity(String.class);
            throw new Exception(message);
        }
    }

    /**
     *
     * @param customerId    String customer ID
     * @return              String customer ID
     * @throws Exception
     */
    public String deleteCustomer(String customerId) throws Exception {
        Response response = baseUrl
                .path("/customers/" + customerId)
                .queryParam("customerId", customerId)
                .request(MediaType.APPLICATION_JSON)
                .delete();

        if (response.getStatus() == 200) {
            return response.readEntity(String.class);
        } else {
            String message = response.readEntity(String.class);
            throw new Exception(message);
        }
    }

    public List<String> getCustomerReport(String customerId, Timestamp from, Timestamp to) throws Exception {
        Response response = baseUrl
                .path("/customers/" + customerId +"/customerReport" )
                .queryParam("from", from.toString())
                .queryParam("to", to.toString())
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {

            return response.readEntity(new GenericType<List<String>>() {
            });
        } else {
            System.out.println(response.getStatus());
            String message = response.readEntity(String.class);
            throw new Exception(message);
        }
    }


}

