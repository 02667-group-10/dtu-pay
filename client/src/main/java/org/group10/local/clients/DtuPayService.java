package org.group10.local.clients; /**
 * @author: Simon James Jensen
 */

import org.group10.local.models.Customer;
import org.group10.local.models.Merchant;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
/**
 * DTU Pay Service functions as a controller object for the client-side application
 * It instantiates the two client-object and defines functions to communicate with them.
 * It generates and returns new Customer and Merchant objects, as well as return response entities from the
 * org.group10.local.clients.
 * Exceptions are passed back to be handled by the View
 */
public class DtuPayService {

    CustomerClient customerClient;
    MerchantClient merchantClient;
    public Merchant merchant;
    public Customer customer;

    public DtuPayService() {

        customerClient = new CustomerClient();
        merchantClient = new MerchantClient();
    }

    /**
     *
     * @param firstName String first name
     * @param lastName  String last name
     * @param cpr       String cpr
     * @throws Exception
     */
    public void createCustomer(String firstName, String lastName, String cpr) throws Exception {
        this.customer = new Customer(firstName, lastName, cpr);
        String ID = customerClient.createCustomer(customer);
        customer.setID(ID);
        List<String> newTokens = customerClient.getTokens(customer.getId(), 6);
        customer.addTokens(newTokens);

    }

    /**
     *
     * @param firstName String first name
     * @param lastName  String last name
     * @param cpr       String cpr
     * @throws Exception
     */
    public void createMerchant(String firstName, String lastName, String cpr) throws Exception {
        this.merchant = new Merchant(firstName, lastName, cpr);
        String ID = (merchantClient.createMerchant(merchant));

        merchant.setId(ID);
    }

    /**
     *
     * @param id  String merchant Id
     * @throws Exception
     */
    public void deleteMerchant(String id) throws Exception {
        this.merchant = null;
        merchantClient.deleteMerchant(id);
    }

    /**
     *
     * @param id String customer Id
     * @throws Exception
     */
    public void deleteCustomer(String id) throws Exception {
        this.customer = null;
        customerClient.deleteCustomer(id);
    }

    /**
     *
     * @param merchantId    String merchant ID
     * @param token         String customer token
     * @param amount        String amount of money
     * @return              String merchant ID
     * @throws Exception
     */
    public String requestPayment(String merchantId, String token, String amount) throws Exception {
        return this.merchantClient.createTransfer(merchantId, token, amount);
    }

    /**
     *
     * @param customerId        String merchant ID
     * @param amountOfTokens    int amount of tokens requested
     * @return                  List<String> tokens
     * @throws Exception
     */
    public List<String> requestTokens(String customerId, int amountOfTokens) throws Exception {
        Integer amountToken = customer.getAmountOfTokens();
        if (amountToken<=1 && amountToken<=5) {
            return customerClient.getTokens(customerId, amountOfTokens);}

        return new ArrayList<>();
    }

    public List<String> requestCustomerReport(String customerId, Timestamp from, Timestamp to) throws Exception {
        return this.customerClient.getCustomerReport(customer.getId(), from, to);
    }

    public List<String> requestMerchantReport(String merchantId, Timestamp from, Timestamp to) throws Exception {
        return this.merchantClient.getMerchantReport(merchantId, from, to);
    }


}
