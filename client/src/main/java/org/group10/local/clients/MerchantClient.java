package org.group10.local.clients; /**
 * @author: Simon James Jensen
 */

import org.group10.local.models.Merchant;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;

/**
 * org.group10.local.clients.CustomerClient functions as the client for calls customer-path of the rest API
 * It builds a new client and routes function calls to the relevant rest API path.
 * Entities are built from arguments and depending on the nature of the response object data is either returned
 * or Exceptions are passed back to be handled by the instantiating class.
 */
public class MerchantClient {
    WebTarget baseUrl;

    public MerchantClient() {
        Client client = ClientBuilder.newClient();
        baseUrl = (WebTarget) client.target("http://localhost:8007"); //to connect from local client to server: http://g-10.compute.dtu.dk:8007
    }

    /**
     * @param merchant Merchant object
     * @return String merchant ID
     * @throws Exception
     */
    public String createMerchant(Merchant merchant) throws Exception {
        Response response = baseUrl
                .path("/merchants")
                .queryParam("firstName", merchant.getFirstName())
                .queryParam("lastName", merchant.getLastName())
                .queryParam("cpr", merchant.getCpr())
                .request(MediaType.APPLICATION_JSON)
                .post(null);
        if (response.getStatus() == 200) {
            return response.readEntity(String.class);
        } else {
            String message = response.readEntity(String.class);
            throw new Exception(message);
        }

    }

    /**
     * @param merchantId String merchant ID
     * @param token      String customer token
     * @param amount     String amount of money
     * @return String merchant ID
     * @throws Exception
     */
    public String createTransfer(String merchantId, String token, String amount) throws Exception {
        Response res = baseUrl
                .path("/merchants/" + merchantId + "/payment")
                .queryParam("token", token)
                .queryParam("amount", amount)
                .request(MediaType.APPLICATION_JSON)
                .post(null);
        if (res.getStatus() == 200) {
            return res.readEntity(String.class);
        } else {
            String message = res.readEntity(String.class);
            throw new Exception(message);
        }
    }

    /**
     * @param merchantId String merchant ID
     * @return String merchant ID
     * @throws Exception
     */
    public String deleteMerchant(String merchantId) throws Exception {
        Response response = baseUrl
                .path("/merchants/" + merchantId)
                .queryParam("merchantId", merchantId)
                .request(MediaType.APPLICATION_JSON)
                .delete();

        if (response.getStatus() == 200) {
            return response.readEntity(String.class);
        } else {
            String message = response.readEntity(String.class);
            throw new Exception(message);
        }
    }

    public List<String> getMerchantReport(String merchantId, Timestamp from, Timestamp to) throws Exception {
        Response response = baseUrl
                .path("/merchants/" + merchantId +"/merchantReport" )
                .queryParam("from", from.toString())
                .queryParam("to", to.toString())
                .request(MediaType.APPLICATION_JSON)
                .get();
        if (response.getStatus() == 200) {

            return response.readEntity(new GenericType<List<String>>() {
            });
        } else {
            String message = response.readEntity(String.class);
            throw new Exception(message);
        }
    }
}
