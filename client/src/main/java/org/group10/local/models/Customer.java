package org.group10.local.models;

import java.util.ArrayList;
import java.util.List;

// @Author: Simon James Jensen
public class Customer extends Client {

    private List<String> tokens;

    public Customer(String firstName, String lastName, String cpr) {
        super(firstName, lastName, cpr);
        this.tokens = new ArrayList<>();
    }

    public void addTokens(List<String> tokens) {
        this.tokens.addAll(tokens);
    }

    public String giveToken() {
        if (tokens.size() > 0)
            return tokens.remove(0);
        return null;
    }
    public int getAmountOfTokens() {
        return this.tokens.size();
    }



}