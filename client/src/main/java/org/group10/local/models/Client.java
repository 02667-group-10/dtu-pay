/**
 * @author: Mathias Søndergaard
 */
package org.group10.local.models;

public abstract class Client {
    public String id;
    public String firstName;
    public String lastName;
    public String cpr;


    public Client(String firstName, String lastName, String cpr){
        this.firstName = firstName;
        this.lastName = lastName;
        this.cpr = cpr;
    }
    public void setId(String ID){ this.id = ID;}

    public String getId() {
        return this.id;
    }

    public void setID(String ID) {this.id = ID;}


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getCpr() {
        return cpr;
    }



    @Override
    public String toString(){
        return "cpr: " + cpr + " ID: " + id + " name: " + firstName + " surname: " + lastName;
    }

    @Override
    public boolean equals(Object Client){
        org.group10.local.models.Client C = (org.group10.local.models.Client) Client;

        return ((this.id.equals(C.id)) && (this.lastName.equals(C.lastName)) &&
                (this.firstName.equals(C.firstName)) && (this.cpr.equals(C.cpr)));
    }
}

