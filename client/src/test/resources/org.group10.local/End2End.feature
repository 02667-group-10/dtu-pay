Feature: End 2 End Testing

  Scenario: Successful Customer registration
    When a Customer named "Luke" "Skywalker" CPR: "1234567890" registers
    Then a new Customer named "Luke" "Skywalker" CPR: "1234567890" is created
    And the Customer has an ID which is not "-1"
    And the Customer has 6 unused tokens

  Scenario: Successful Merchant registration
    When a Merchant named "Han" "Solo" CPR: "2345678901" registers
    Then a new Merchant named "Han" "Solo" CPR: "2345678901" is created
    And the Merchant has an ID which is not "-1"

  Scenario: Successful payment
    Given a Customer named "Leia" "Organa" CPR: "3456789012" registers
    And a Merchant named "Chewbacca" "of Kashyyyk" CPR: "4567890123" registers
    When the Merchant requests a payment of 10 kr. from the Customer
    Then the payment is made
    And the Customer has 5 unused tokens


  Scenario: Successful token request
    Given a Customer named "Biggs" "Darklighter" CPR: "5678901234" registers
    And the Customer has 1 unused tokens after he threw away 5 other
    When the Customer requests 5 new tokens
    Then the Customer has 6 unused tokens

  Scenario: Incrementing IDs
    When a Customer named "Bib" "Fortuna" CPR: "6789012345" registers
    And a Merchant named "Jango" "Fett" CPR: "7890123456" registers
    Then the Customers ID will be different from the Merchants ID

  Scenario: Delete Customer
    When a Customer named "Jabba" "the Hutt" CPR: "0123456789" registers
    And The same customer is deleted
    Then The customer is deleted

  Scenario: Delete Merchant
    When a Merchant named "Darth" "Vader" CPR: "0234567891" registers
    And The same merchant is deleted
    Then The merchant is deleted


  Scenario: Unsuccessful payment (unregistered merchant)
    Given a Customer named "Wedge" "Antilles" CPR: "3000000000" registers
    And a Merchant named named "Lando" "Calrissian" CPR: "4000000000" is NOT registered
    And the Customer has 6 unused tokens
    When the Merchant requests a payment of 10 kr. from the Customer
    Then an error message "User not found" is received

  Scenario: Unsuccessful token request
    Given a Customer named "Boba" "Fett" CPR: "8901234567" registers
    And the Customer has 6 unused tokens
    When the Customer requests 4 new tokens
    Then the Customer has 6 unused tokens

  Scenario: Successful Customer report
    Given a Customer named "Brian" "John" CPR: "1234567899" registers
    And   a Merchant named "Han" "Solo" CPR: "2345678901" registers
    When the Merchant requests a payment of 10 kr. from the Customer
    When the customer wants his report
    Then a report is fetched containing 1 item

  Scenario: Successful Merchant report
    Given a Customer named "Leia" "Organa" CPR: "3456789012" registers
    And a Merchant named "Bobby" "Brown" CPR: "0234567822" registers
    When the Merchant requests a payment of 10 kr. from the Customer
    When the Merchant wants his report
    Then a report is fetched containing 1 item