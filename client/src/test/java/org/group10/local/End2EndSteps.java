/**
 * @author: Simon James Jensen
 */
package org.group10.local;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.group10.local.clients.DtuPayService;
import org.group10.local.models.Merchant;

import java.sql.Timestamp;
import java.util.List;

import static org.junit.Assert.*;

public class End2EndSteps {
    private DtuPayService dtuPayService = new DtuPayService();
    private String successfulPaymentRequest;
    private String errorMessage;
    private List<String> report;


    @When("a Customer named {string} {string} CPR: {string} registers")
    public void aCustomerNamedCPRRegistersSuccessfully(String firstName, String lastName, String cpr) {
        try {
            dtuPayService.createCustomer(firstName, lastName, cpr);
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = e.getMessage();
        }
    }

    @Then("a new Customer named {string} {string} CPR: {string} is created")
    public void aNewCustomerNamedCPRIsCreated(String firstName, String lastName, String cpr) {
        assertEquals(firstName, dtuPayService.customer.getFirstName());
        assertEquals(lastName, dtuPayService.customer.getLastName());
        assertEquals(cpr, dtuPayService.customer.getCpr());
    }

    @When("a Merchant named {string} {string} CPR: {string} registers")
    public void aMerchantNamedCPRRegisters(String firstName, String lastName, String cpr) {
        errorMessage = null;
        try {
            dtuPayService.createMerchant(firstName, lastName, cpr);
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = e.getMessage();
        }
    }

    @Then("a new Merchant named {string} {string} CPR: {string} is created")
    public void aNewMerchantNamedCPRIsCreated(String firstName, String lastName, String cpr) {
        assertEquals(firstName, dtuPayService.merchant.getFirstName());
        assertEquals(lastName, dtuPayService.merchant.getLastName());
        assertEquals(cpr, dtuPayService.merchant.getCpr());
    }

    @Given("the Customer has {int} unused tokens after he threw away {int} other")
    public void the_customer_has_unused_tokens_after_he_threw_away_other(Integer after_tokens, Integer popTokens) {
        for (int x = 0; x < popTokens; x++) {
            dtuPayService.customer.giveToken();
        }
        assertEquals((int) after_tokens, dtuPayService.customer.getAmountOfTokens());
    }

    @Then("the Customer has an ID which is not {string}")
    public void theCustomerHasAnIDWhichIsNot(String id) {
        assertNotEquals(id, dtuPayService.customer.getId());
    }

    @Then("the Merchant has an ID which is not {string}")
    public void theMerchantHasAnIDWhichIsNot(String id) {
        assertNotEquals(id, dtuPayService.merchant.getId());
    }

    @Then("the Customer has {int} unused tokens")
    public void theCustomerHasUnusedTokens(int tokens) {
        assertEquals(dtuPayService.customer.getAmountOfTokens(), tokens);
    }

    @When("the Merchant requests a payment of {int} kr. from the Customer")
    public void theMerchantRequestsAPaymentFromTheCustomer(int kr) {
        errorMessage = null;
        try {
            this.successfulPaymentRequest = dtuPayService.requestPayment(dtuPayService.merchant.getId(), dtuPayService.customer.giveToken(), Integer.toString(kr));
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = e.getMessage();
        }
    }

    @Then("the payment is made")
    public void thePaymentIsMade() {
        // If we get this far, no error has occured (an execption in client would have been thrown),
        // this check is simply a dummy check.
        // Should have been refactored, but time constraints.
        assertNotNull(this.successfulPaymentRequest);
    }

    @When("the Customer requests {int} new tokens")
    public void theCustomerRequestsIntNewTokens(int tokens) {
        try {
            dtuPayService.customer.addTokens(dtuPayService.requestTokens(dtuPayService.customer.getId(), tokens));
        } catch (Exception e) {
            e.printStackTrace();
            errorMessage = e.getMessage();
        }
    }

    @Then("an error message {string} is received")
    public void anErrorMessageIsReceived(String error) {
        assertEquals(error, errorMessage);
    }

    @And("a Merchant named is unregistered")
    public void aMerchantNamedIsUnregistered() {
        dtuPayService.merchant = null;
    }

    @And("a Merchant named named {string} {string} CPR: {string} is NOT registered")
    public void aMerchantNamedNamedCPRIsUnregistered(String firstName, String lastName, String cpr) {
        dtuPayService.merchant = new Merchant(firstName, lastName, cpr);
        dtuPayService.merchant.setId("5000");
    }

    @Given("a Customer named {string} {string} CPR: {string} is NOT registered")
    public void aCustomerNamedCPRIsNOTRegistered(String firstName, String lastName, String cpr) {
        dtuPayService.merchant = new Merchant(firstName, lastName, cpr);
        dtuPayService.merchant.setId("6000");
    }

    @Then("the Customers ID will be different from the Merchants ID")
    public void theCustomersIDWillBeDifferentFromTheMerchantsID() {
        assertNotEquals(dtuPayService.customer.getId(), dtuPayService.merchant.getId());
    }

    @When("The same customer is deleted")
    public void the_same_customer_is_deleted() throws Exception {
        dtuPayService.deleteCustomer(dtuPayService.customer.getId());
    }

    @Then("The customer is deleted")
    public void the_customer_is_deleted() {
        assertEquals(dtuPayService.customer, null);
    }

    @Then("an error message is received")
    public void anErrorMessageIsReceived() {
        System.out.println(errorMessage);
        assertNotNull(errorMessage);
    }

    @And("The same merchant is deleted")
    public void theSameMerchantIsDeleted() throws Exception {
        dtuPayService.deleteMerchant(dtuPayService.merchant.getId());
    }

    @Then("The merchant is deleted")
    public void theMerchantIsDeleted() {
        assertEquals(dtuPayService.merchant, null);
    }

    @Given("a previous transaction occured")
    public void a_previous_transaction_occured() {

        this.thePaymentIsMade();

    }
    @When("the customer wants his report")
    public void the_customer_wants_his_report() throws Exception {
        report = dtuPayService.requestCustomerReport(dtuPayService.customer.getId(), Timestamp.valueOf
                ("2012-01-13 13:13:13.013"), Timestamp.valueOf("2022-01-13 13:13:13.013"));    }
    @Then("a report is fetched containing {int} item")
    public void a_report_is_fetched_containing_item(Integer length) {
        assertEquals((int)length, report.size() /5);
    }
    @When("the Merchant wants his report")
    public void the_merchant_wants_his_report() throws Exception {
        report = dtuPayService.requestMerchantReport(dtuPayService.merchant.getId(), Timestamp.valueOf
                ("2012-01-13 13:13:13.013"), Timestamp.valueOf("2022-01-13 13:13:13.013"));

    }

}
