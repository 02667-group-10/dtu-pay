import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

/* Important: 
for Cucumber tests to be recognized by Maven, the class name has to have
either the word Test in the beginning or at the end. 
For example, the class name CucumberTests (Test with an s) will be ignored by Maven.
*/


@io.cucumber.junit.platform.engine.Cucumber // comment this line for local testing (Mathias PC)
//@RunWith(Cucumber.class) // Out comment this line for local testing (Mathias PC)
@CucumberOptions(features="src/test/resources") // tells it the path of the features (, glue = "src/test/java")
public class CucumberTest {
}
